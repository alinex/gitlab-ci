# Build NodeJS app

This template will build the NodeJS application and make it available as artifact.

It will call `npm install` and `npm run build` and make the following folders available as artifact: `bin`, `lib`, `node_modules` and `dist`.

Instead of a general `build` script you can also use only specific builds using the network shortname as appendix:
- `build:test` -> into `dist/test`
- `build:int` -> into `dist/int`
- `build:stage` -> into `dist/stage`
- `build:prod` -> into `dist/prod`

This will lead to environment specific builds which will be called depending on the possible upload jobs.

It will run after all tests are done.

## Jobs

| Job          | Pipeline | Precondition                  | Tool                         | Report Artifact |
| ------------ | -------- | ----------------------------- | ---------------------------- | --------------- |
| `unit-mocha` | branch   | `test/unit/mocha/*.js + *.ts` | [Mocha](https://mochajs.org) | -               |
| `build-node` | branch   | `test/unit/bats/*.bats`       | npm                          | \<dirctories>   |

### Configuration

| Variable      | Default | Example                               | Description        |
| ------------- | ------- | ------------------------------------- | ------------------ |
| `EXCLUDE_JOB` |         | `build-node`, `node`, `unit`, `mocha` | Exclude these jobs |

## Example

The job is added using the default templates:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/main.yml
```
