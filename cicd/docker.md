# Build Docker image using kaniko

This template will build and push docker images to registry.

## Jobs

| Job               | Pipeline | Precondition | Image                                                                               | Tool                                                                      | Artifacts    |
| ----------------- | -------- | ------------ | ----------------------------------------------------------------------------------- | ------------------------------------------------------------------------- | ------------ |
| `build-docker`    | branch   | `Dockerfile` | [docker-build](https://gitlab-intern.ekz.de/templates/images-ci/docker-build)       | [kaniko](https://gitlab-intern.ekz.de/templates/images-ci/docker-build)   | `docker.tar` |
| `security-docker` | branch   | `Dockerfile` | [docker-security](https://gitlab-intern.ekz.de/templates/images-ci/docker-security) | [trivy](https://gitlab-intern.ekz.de/templates/images-ci/docker-security) | -            |
| `push-docker`     | branch   | `Dockerfile` | [docker-push](https://gitlab-intern.ekz.de/templates/images-ci/docker-push)         | [Crane](https://gitlab-intern.ekz.de/templates/images-ci/docker-push)     | -            |


### Configuration

| Variable      | Default | Example                                    | Description      |
| ------------- | ------- | ------------------------------------------ | ---------------- |
| `EXCLUDE_JOB` |         | `/build/`, `/build-docker/`, `/docker/`... | Exclude this job |

This will use the general configuration of `CI_REGISTRY`, `CI_REGISTRY_USER` and `CI_REGISTRY_PASSWORD`, which will be set by GitLab if you enable the registry, to authenticate and connect to the docker registry.

## Example

The job is added using the default templates:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/main.yml
```
