# Sending messages to Mattermost

This template will define a hidden base job which will help you to send messages.

It will include some default jobs, but also allows to define more easily.

## Jobs

Predefined Jobs:

| Job                        | Precondition | Format   |
| -------------------------- | ------------ | -------- |
| `mattermost-success-test`  | success      | Markdown |
| `mattermost-failure-test`  | failure      | Markdown |
| `mattermost-success-stage` | success      | Markdown |
| `mattermost-failure-stage` | failure      | Markdown |
| `mattermost-success-prod`  | success      | Markdown |
| `mattermost-failure-prod`  | failure      | Markdown |

### Configuration

| Variable                   | Default | Example                                                        | Description                      |
| -------------------------- | ------- | -------------------------------------------------------------- | -------------------------------- |
| `EXCLUDE_JOB`              |         | `/mattermost/`, `/mattermost-success/`, `/mattermost-failure/` | Exclude this job                 |
| `TEST_SUCCESS_MATTERMOST`  |         | `status-test`                                                  | space separated list of channels |
| `TEST_FAILURE_MATTERMOST`  |         | `status-test`                                                  | space separated list of channels |
| `INT_SUCCESS_MATTERMOST`   |         | `status-integration`                                           | space separated list of channels |
| `INT_FAILURE_MATTERMOST`   |         | `status-integration`                                           | space separated list of channels |
| `STAGE_SUCCESS_MATTERMOST` |         | `status-staging`                                               | space separated list of channels |
| `STAGE_FAILURE_MATTERMOST` |         | `status-staging`                                               | space separated list of channels |
| `PROD_SUCCESS_MATTERMOST`  |         | `status-produktion`                                            | space separated list of channels |
| `PROD_FAILURE_MATTERMOST`  |         | `status-produktion`                                            | space separated list of channels |

Only possible for own Mattermost job:

| Variable             | Default | Example               | Description                      |
| -------------------- | ------- | --------------------- | -------------------------------- |
| `MATTERMOST_CHANNEL` |         | `status-produktion`   | space separated list of channels |
| `MESSAGE`            |         | `This is only a test` | markdown content                 |

## Example

It is automatically included using `main.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/main.yml
```
