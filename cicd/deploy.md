# SSH Upload/Deploy

This template will deploy using a SSH connection. 

## Environments

It supports the use of environments within the deploy jobs. Therefore a setting like `environment: test` is included in any deployment/review job.
The environment names are: `test`, `int`, `stage` and `prod`.

At the moment scoped variables are only used within the application itself, not in the general flow control of deployment. Here we further use prefixed variables.

## Upload

The upload will 
- generate the distribution directory
- optimize config folder
  - resolving network and host folders into base dir
  - replacing containing environment variables
- generate the activate script
- cleanup old uploads 
- copy the new files onto the server, but didn't activate it

Precondition is that the key under `/home/gitlab-runner/.ssh/id_rsa` is also added as authorized key in all the target hosts (admin user).

Within the given `UPLOAD_FOLDER` a directory named `<project>-<tag>-<pipeline-id>` will be created. It also includes a `version.info` file.

```bash
$ ll /opt/divibib/upload
drwxr-xr-x 4 admin admin 4096 17. Mär 14:54 gitlab-ci-v0.13.5-22166/
-rw-r--r-- 1 admin admin 1219 17. Mär 15:56 gitlab-ci-v0.13.5-22166.activate
```

```text title="version.info"
NAME        betrieb/operations
REVISION    1b692ea535794e79449f8cef029948a933aea7c8
TAG         v1.9.0
LANGUAGES   typescript,shell,javascript,vim script
PIPELINE    https://gitlab.com/alinex/operations/-/pipelines/4728
USER        alinex <info@alinex.de>
DATE        2021-12-30T14:35:08+01:00
PROJECT     https://gitlab.com/alinex/operations
REPOSITORY  http://gitlab-ci-token:[MASKED]@gitlab.com/alinex/operations.git
```

You can either call the activate script on the servers or let this be done by the activate Job in the CI/CD.

## Activate

Within the deployment the following steps will be done:

-   fail on missing preconditions (commands, services, paths)
-   check if directory is still there
-   run pre jobs
-   if other parts than *.war are present
    -   copy directory
    -   replace current with new version
    -   make link into log directory
    -   write install log
-   if it contains *.war and tomcat is installed run for each war file
    -   undeploy old application
    -   set `spring.config.location` as environment variable (if config exists)
    -   deploy new application
    -   handle f5-status
-   run post jobs

All this is already coded into the activate script by the upload job. This job will only execute it.

While within a freeze time (set in Settings > CI/CD > Deploy freezes) the production deploy will be not made automatically and you have to do this manually.

The process is also logged locally under `/var/log/<namespace>/cicd.log`:

```bash
$ tail /var/log/divibib/cicd.log
# 2023-03-17 15:56:58 gitlab-ci-v0.13.5-22166 deployed by Alexander Schilling
/opt/divibib/upload/gitlab-ci-v0.13.5-22166.activate
```

## Quality Gates

To prevent code deployment with vulnerabilities quality gates are defined. They are set in the checks like sast-*, solar or docker-security and evaluated before deployment. The following flag variables will be empty if ok or has `1`  if it should block:

- `$TEST_BLOCKED` 
- `$INT_BLOCKED` 
- `$STAGE_BLOCKED` 
- `$PROD_BLOCKED` 

Additional information can be found in the file `<job>.blocked` contain something like `docker-security: Input buffer over-read in AES-XTS implementation on 64 bit ARM (CVE-2023-1255)`.

If this occures you have two possibilities:
1. fix the code - always the best
2. disable the check - may help only in a single run and is risky
3. add the Problem (shown at the end of message) to the exclude list

The exclude List `QUALITY_GATE_EXCLUDE` can be set to a space separated List of identifiers. **But this should be used only temporary and sparsely.**

## Jobs

| Stage/Netz   | Jobs                                  | Condition                       | Host Pattern                               |
| ------------ | ------------------------------------- | ------------------------------- | ------------------------------------------ |
| **test-rt**  | `upload-test-rt` `activate-test-rt`   | develop, feature branch         | `/\.grp/`                                  |
| **test-ps**  | `upload-test-ps` `activate-test-ps`   | disabled                        |                                            |
| **int-rt**   | `upload-int-rt` `activate-int-rt`     | `CI_COMMIT_BRANCH == "develop"` | `/\.grp/`                                  |
| **int-ps**   | `upload-int-ps` `activate-int-ps`     | disabled                        |                                            |
| **stage-rt** | `upload-stage-rt` `activate-stage-rt` | main, release, hotfix branch    | `/\.(staging\.dvb\|grp)/`                  |
| **stage-ps** | `upload-stage-ps` `activate-stage-ps` | main, release, hotfix branch    | `/\.stage\.dvb/`                           |
| **prod-rt**  | `upload-prod-rt` `activate-prod-rt`   | main branch                     | ` /\.(user\|customer\|office)\.grp`        |
| **prod-ps**  | `upload-prod-ps` `activate-prod-ps`   | main branch                     | `/\.(cloud\|user\|customer\|office)\.dvb/` |

### Configuration

| Variable                   | Default                          | Example                                               | Description                                                                       |
| -------------------------- | -------------------------------- | ----------------------------------------------------- | --------------------------------------------------------------------------------- |
| `EXCLUDE_JOB`              |                                  | `/upload/`, `/deploy/`, `/upload-stage/`...           | Exclude this job                                                                  |
| `EXCLUDE_HOST`             |                                  | `/\.staging.dvb/`                                     | Host to exclude from deployment.                                                  |
| `EXCLUDE_HOST_DESCRIPTION` |                                  | `no deployments to staging environment while testing` | A text to display if blocked.                                                     |
| `NAMESPACE`                | `<gitlab group>`                 | `divibib`                                             | Used as template for paths in automatic detection.                                |
| `UPLOAD_PARTS`             | * see below                      |                                                       | Directories or files to upload (if no dist dir)                                   |
| `UPLOAD_KEEP_OLD`          | 3                                |                                                       | Number of old uploads to keep.                                                    |
| `UPLOAD_FOLDER`            | `/opt/$NAMESPACE/upload`         |                                                       | Folder the code will be uploaded to.                                              |
| `INSTALLED_COMMANDS`       |                                  | `java`                                                | Check that the commands are available before deploying.                           |
| `RUNNING_SERVICES`         |                                  | `tomcat8@tomcat8_1`                                   | Check that the services are active before deploying.                              |
| `READABLE_PATHS`           |                                  | `/var/lib/test`                                       | These paths has to be readable.                                                   |
| `WRITABLE_PATHS`           |                                  | `/tmp`                                                | These paths has to be writable.                                                   |
| `BEFORE_DEPLOY`            |                                  | `sudo systemctl stop myapp`                           | Run this additional command before deploying (may use `\$DEPLOY_PATH`).           |
| `HTTP_CHECK`               |                                  | `/actuator/health\|401,2*\|{"status":"up"}`           | HTTP checks to ensure it is really running                                        |
| `HTTP_BASIC_AUTH`          |                                  | `user:password`                                       | If HTTP check needs basic auth                                                    |
| `MAX_STARTUP_TIME`         | 30                               |                                                       | Time in seconds till service should be running.                                   |
| `AFTER_DEPLOY`             |                                  | `sudo systemctl start myapp`                          | If set the softlink for the log files will not be made.                           |
| `NO_LOG_FOLDER`            |                                  | 1                                                     | If set an application directory under the defined path is defined and softlinked. |
| `PERSISTENT_DATA_PATH`     |                                  | `/data`                                               | Run this additional command after deploying (may use `\$DEPLOY_PATH`).            |
| `TEST_SSH`                 |                                  | `admin@operations.host.test.dvb`                      | SSH connection string list                                                        |
| `TEST_DEPLOY_PATH`         | `/opt/$NAMESPACE/<project-name>` |                                                       | Path there the application will be deployed.                                      |
| `TEST_TOMCAT_INST`         | 0                                | 1                                                     | Instance number for multi-tomcat install.                                         |
| `TEST_TOMCAT_USER`         |                                  | `jenkins`                                             | Username to deploy on tomcat.                                                     |
| `TEST_TOMCAT_PASS`         |                                  | `????????`                                            | Password to deploy on tomcat.                                                     |
| `TEST_TOMCAT_PASS`         |                                  | `????????`                                            | Password to deploy on tomcat.                                                     |
| `TEST_TOMCAT_RESTART`      |                                  | 1                                                     | Set to any value to do a restart on each deploy.                                  |
| `INT_SSH`                  |                                  | `admin@operations.host.int.dvb`                       | SSH connection string list                                                        |
| `INT_DEPLOY_PATH`          | `/opt/$NAMESPACE/<project-name>` |                                                       | Path there the application will be deployed.                                      |
| `INT_TOMCAT_INST`          | 0                                | 1                                                     | Instance number for multi-tomcat install.                                         |
| `INT_TOMCAT_USER`          |                                  | `jenkins`                                             | Username to deploy on tomcat.                                                     |
| `INT_TOMCAT_PASS`          |                                  | `????????`                                            | Password to deploy on tomcat.                                                     |
| `INT_TOMCAT_RESTART`       |                                  | 1                                                     | Set to any value to do a restart on each deploy.                                  |
| `STAGE_SSH`                |                                  | `admin@operations.host.stage.dvb`                     | SSH connection string list                                                        |
| `STAGE_DEPLOY_PATH`        | `/opt/$NAMESPACE/<project-name>` |                                                       | Path there the application will be deployed.                                      |
| `STAGE_TOMCAT_INST`        | 0                                | 1                                                     | Instance number for multi-tomcat install.                                         |
| `STAGE_TOMCAT_USER`        |                                  | `jenkins`                                             | Username to deploy on tomcat.                                                     |
| `STAGE_TOMCAT_PASS`        |                                  | `????????`                                            | Password to deploy on tomcat.                                                     |
| `STAGE_TOMCAT_RESTART`     |                                  | 1                                                     | Set to any value to do a restart on each deploy.                                  |
| `PROD_SSH`                 |                                  | `admin@operations.host.test.dvb`                      | SSH connection string list                                                        |
| `PROD_DEPLOY_PATH`         | `/opt/$NAMESPACE/<project-name>` |                                                       | Path there the application will be deployed.                                      |
| `PROD_TOMCAT_INST`         | 0                                | 1                                                     | Instance number for multi-tomcat install.                                         |
| `PROD_TOMCAT_USER`         |                                  | `jenkins`                                             | Username to deploy on tomcat.                                                     |
| `PROD_TOMCAT_PASS`         |                                  | `????????`                                            | Password to deploy on tomcat.                                                     |
| `PROD_TOMCAT_RESTART`      |                                  | 1                                                     | Set to any value to do a restart on each deploy.                                  |
| `PROD_AUTODEPLOY`          |                                  | `true`                                                | auto deploy to production if not in freeze time                                   |
| `DEPLOY_WAIT`              | 20                               |                                                       | Seconds to wait bfore additional node (cluster).                                  |

The `UPLOAD_PARTS` define what to deploy. If nothing is set it will use the following: `bin config data lib node_modules package.json`
If `config` is given it will also move subfolders directly to the `config` folder:
- all files under `config/<NETWORK>`
- all files under `config/host/<HOSTNAME>`

All Settings with no default value need to be provided if the specific jobs should run.
The `PROD_*`, `STAGE_*`, `INT_*` and `TEST_*` variables should be defined in the UI. There you can set multiple `*_SSH` paths in a variable separated by space or newlines.

Within the `BEFORE_DEPLOY` and `AFTER_DEPLOY` scripts you can use the following variable: `$$DEPLOY_PATH`. If the process is more complex think about adding a script to your distribution, which you call here.

## Activation

The activation script is auto generated for each deploy job. So it can contain the needed parts like:

- Check if `$INSTALLED_COMMANDS` are installed
- Check if `$RUNNING_SERVICES` are really running
- Check if `$READABLE_PATHS` are correct
- Check if `$WRITABLE_PATHS` are correct
- Check if systemd configuration is up to date
- Run `$BEFORE_DEPLOY` script
- Stopping systemd service
- General deploy (Replace Destination Folder with the contents from deployment))
- Tomcat deploy
- Docker deploy
- Add log entry
- Add persistent data entry
- Starting systemd service
- Setup logrotate
- Run `$AFTER_DEPLOY` script

## Example

This is loaded by `main.yml` but you need to configure the variables to be activated, too.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/main.yml
```

## Preconditions

To make this work an OpenSSH connection is needed. To establish this a private key has to be set in each GitLab runner's user account and the adjoining public key has to be added on all user accounts and machines to which you may deploy. Add it to the `authorized_keys` list as usual.

### Network auto detection

Also because of network restrictions and that not all machines are directly reachable from the main gitlab runners we support specific runners in other computing centers. They are triggert using the tags in the specific jobs. But to use the right jobs for the right deploy destinations the autodetection has to be setup correct. This is done using regular expressions within the `*_ENABLE` and `*_FILTER` variables within the [deploy.yml](deploy.yml) based on the domain names.

In detail the following steps are needed to make the whole setup work:
1. public `<network>_*` variables are defined for customization within each project.
2. definition of `<network>_<computing-center>_ENABLE` and `<network>_<computing-center>_FILTER`
3. using the filter setting within the script

This allows to enable all jobs neccessary for any of the defined hosts and run in each only the elements contained in the specific computing center.
