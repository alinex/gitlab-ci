# Print all environment variables

This is helpful to write your CI Scripts.
It will print all environment variables which are set. This may help to find problems in template structure.

Known Problems:

-   config structure failures may prevent default settings
-   variables may not be defined in project and not checked within template

## Jobs

| Job        | Pipeline | Precondition | Tool | Report Artifact |
| ---------- | -------- | ------------ | ---- | --------------- |
| `printenv` | all      | -            | -    | -               |

As a result you can find the output of all variables in the `printenv` job log:

```bash
...
export CI_SERVER_VERSION_MAJOR='14'
export CI_SERVER_VERSION_MINOR='6'
export CI_SERVER_VERSION_PATCH='0'
export DEPLOY_PATH=''
export DEPLOY_WAIT='20'
export TEST_DEPLOY_PATH='/opt/operator'
...
```

## Configuration

### Configuration

| Variable      | Default | Example      | Description                                            |
| ------------- | ------- | ------------ | ------------------------------------------------------ |
| `INCLUDE_JOB` |         | `/printenv/` | Include this job                                       |
| `DEBUG`       |         | `1`          | Debug mode will also enable this step if not excluded. |
| `EXCLUDE_JOB` |         | `/printenv/` | Exclude from Debug mode                                |

## Examples

By default this job will not run, you have to activate it using `INCLUDE_JOB`:

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/main.yml

variables:
    INCLUDE_JOB: /printenv/
```

> The same can be done if you add the comand `export` without arguments as `script` in one of your jobs.
