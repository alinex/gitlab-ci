# Browser Performance Testing

This will execute [ZAP’s Baseline Scan](https://www.zaproxy.org/docs/docker/baseline-scan/) and doesn’t actively attack your application.

But it can be configured to also perform an active scan to attack your application and produce a more extensive security report.

It will use the url from the specific server variables.

## Jobs

| Job                 | Pipeline      | Precondition | Tool                                                                      | Report Artifact        |
| ------------------- | ------------- | ------------ | ------------------------------------------------------------------------- | ---------------------- |
| `performance-test`  | develop       |              | [ZAP’s Baseline Scan](https://www.zaproxy.org/docs/docker/baseline-scan/) | performance-test.json  |
| `performance-stage` | main/master   |              | [ZAP’s Baseline Scan](https://www.zaproxy.org/docs/docker/baseline-scan/) | performance-stage.json |
| `performance-prod`  | protected tag |              | [ZAP’s Baseline Scan](https://www.zaproxy.org/docs/docker/baseline-scan/) | performance-prod.json  |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

## Configuration

You need the same variables as used for accessing:

| Variable                 | Default  | example                                   | Description             |
| ------------------------ | -------- | ----------------------------------------- | ----------------------- |
| `EXCLUDE_JOB`            |          | `/performance/`, `/performance-stage/`... | Exclude this job        |
| `PROD_URL`               |          | `http.//operations.host.test.dvb:4000`    | URL to be called        |
| `STAGE_URL`              |          | `http.//operations.host.staging.dvb:4000` | URL to be called        |
| `TEST_URL`               |          | `http.//operations.host.test.dvb:4000`    | URL to be called        |
| `DAST_FULL_SCAN_ENABLED` | `"true"` | `"false"`                                 | Disable active scanning |

## Example

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/main.yml
```
