# GitLab Pages

This template will deploy the documentation to GitLab pages making it available under `https://<group/user>.pages.office.grp/<project path>`.

The `report` folder will also have an short index.

## Jobs

| Job            | Pipeline | Precondition | Tool   | Report Artifact |
| -------------- | -------- | ------------ | ------ | --------------- |
| `pages-init`   | Branch   |              | Bash   | `ci-pages/`     |
| `pages`        | Branch   |              | Gitlab | `public/`       |
| `pages-deploy` | Branch   |              | Gitlab |                 |

To make it possible to cumulate reports over the different pipelines the `public` path is kept in cache.

The report will also push to the project root pages for protected push but for all other create a sub directory using the branch or tag name.

## Configuration

| Variable      | Default | example | Description      |
| ------------- | ------- | ------- | ---------------- |
| `EXCLUDE_JOB` |         | `pages` | Exclude this job |

## Example

```yaml title=".gitlab-ci.yml"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/cicd/general/pages.yml
```

## Development

The internal used Variables are:

| Variable         | Default                           |
| ---------------- | --------------------------------- |
| `PAGES_CONTEXT`  | $CI_COMMIT_BRANCH/$CI_PIPELINE_ID |
| `PAGES_PATH`     | ci-pages/$PAGES_CONTEXT           |
| `PAGES_KEEP_OLD` | 5                                 |
