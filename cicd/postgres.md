# Sending results to postgres

This template will define a hidden base job which will help you to send messages.

It will include some default jobs, but also allows to define more easily.

## Jobs

Predefined Jobs:

| Job                     | Precondition | Format |
| ----------------------- | ------------ | ------ |
| `postgres-success-prod` | success      | SQL    |
| `postgres-failure-prod` | failure      | SQL    |

### Configuration

Nothing can be configured here.

## Example

It is automatically included using `main.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/main.yml
```
