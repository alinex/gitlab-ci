# Sending email

This template will define a hidden base job which will help you to send emails.

It will include some default jobs, but also allows to define more easily.

## Jobs

Predefined Jobs:

| Job                   | Precondition | Format |
| --------------------- | ------------ | ------ |
| `email-success-test`  | success      | HTML   |
| `email-failure-test`  | failure      | HTML   |
| `email-success-stage` | success      | HTML   |
| `email-failure-stage` | failure      | HTML   |
| `email-success-prod`  | success      | HTML   |
| `email-failure-prod`  | failure      | HTML   |

### Configuration

| Variable              | Default | Example                                         | Description                             |
| --------------------- | ------- | ----------------------------------------------- | --------------------------------------- |
| `EXCLUDE_JOB`         |         | `/email/`, `/email-success/`, `/email-failure/` | Exclude this job                        |
| `TEST_SUCCESS_EMAIL`  |         | `betriebsteam@divibib.com`                      | space separated list of email addresses |
| `TEST_FAILURE_EMAIL`  |         | `betriebsteam@divibib.com`                      | space separated list of email addresses |
| `INT_SUCCESS_EMAIL`   |         | `betriebsteam@divibib.com`                      | space separated list of email addresses |
| `INT_FAILURE_EMAIL`   |         | `betriebsteam@divibib.com`                      | space separated list of email addresses |
| `STAGE_SUCCESS_EMAIL` |         | `betriebsteam@divibib.com`                      | space separated list of email addresses |
| `STAGE_FAILURE_EMAIL` |         | `betriebsteam@divibib.com`                      | space separated list of email addresses |
| `PROD_SUCCESS_EMAIL`  |         | `betriebsteam@divibib.com`                      | space separated list of email addresses |
| `PROD_FAILURE_EMAIL`  |         | `betriebsteam@divibib.com`                      | space separated list of email addresses |

Only possible for own Email job:

| Variable       | Default                       | Example                      | Description         |
| -------------- | ----------------------------- | ---------------------------- | ------------------- |
| `MAIL_TO`      | user which triggered pipeline | `betriebsteam@divibib.com`   | single mail address |
| `MAIL_SUBJECT` | `GitLab CI/CD Mail`           | `Test`                       | subject of the mail |
| `MAIL_HTML`    | `<p>Hello, world!</p>`        | `<p>This is only a test</p>` | html content        |

## Example

It is automatically included using `main.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/main.yml
```

### Own email job

It is automatically included using `main.yml` but you can load it yourself.

```yaml title=".gitlab-ci.yml" hl_lines="5 8 9 10 11"
include:
    - remote: "http://gitlab.service.office.grp/templates/gitlab-ci/raw/main/cicd/email.yml"

test-email:
    extends: .email
    stage: test
    variables:
        MAIL_TO: betriebsteam@divibib.com
        MAIL_SUBJECT: Email 3
        MAIL_HTML: |
            <p>This is only a test</p>
```

As shown above you should extend the included `.email` template and set the variables.
