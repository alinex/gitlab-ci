# Static Application Security Testing

This will check the source code for known vulnerabilities.

## Jobs

| Job                         | Precondition                                            | Tool                 |
| --------------------------- | ------------------------------------------------------- | -------------------- |
| `sast-bandit`               | `*.py`                                                  | bandit               |
| `sast-brakeman`             | `*.rb`, `Gemfile`                                       | brakeman             |
| `sast-flawfinder`           | `*.c`, `*.cpp`                                          | flawfinder           |
| `sast-mobsf`                | `*.apk`, `AndroidManifest.xml`, `*.ipa`, `*.xcodeproj`  | mobsf                |
| `sast-nodejsscan`           | `package.json`                                          | nodejs-scan          |
| `sast-phpcs-security-audit` | `*.php`                                                 | phpcs-security-audit |
| `sast-pmd-apex`             | `*.cls`                                                 | pmd-apex             |
| `sast-semgrep`              | `*.py`, `*.js`, `*.jsx`, `*.ts`, `*.tsx`, `*.c`, `*.go` | semgrep              |
| `sast-sobelow`              | `mix.exs`                                               | sobelow              |
| `sast-spotbugs`             | `*.groovy`, `*.java`, `*.scala`, `*.kt`                 | spotbugs             |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

The collected SAST report uploads to GitLab as an artifact:

-   report with `gl-sast-report.json` file

This can be shown:

-   in the merge request SAST widget
-   in the security dashboard
-   in the vulnerability report

### Configuration

| Variable      | Default | Example                    | Description         |
| ------------- | ------- | -------------------------- | ------------------- |
| `EXCLUDE_JOB` |         | `/sast/`, `/sast-sobelow/` | Include these jobs. |
