# SonarQube Analysis

This will check the source code for known vulnerabilities.

## Jobs

| Job     | Precondition | Tool      | Report Artifact  |
| ------- | ------------ | --------- | ---------------- |
| `sonar` |              | SonarQube | sast-bandit.json |

### Configuration

| Variable      | Default | Example   | Description         |
| ------------- | ------- | --------- | ------------------- |
| `EXCLUDE_JOB` |         | `/sonar/` | Include these jobs. |
