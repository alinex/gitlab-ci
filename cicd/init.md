# Init Template Pipeline

This is the global setup for the pipeline. It will definee variables, setup stages and set some calculated environment variables to be used later.

## Jobs

| Job    | Pipeline | Precondition | Tool  | Report Artifact |
| ------ | -------- | ------------ | ----- | --------------- |
| `init` | All      |              | Shell | ci-env/         |

## Layers

The following data layers will be created as directories:
- ci-env - for environment files like `$CI_JOB_NAME.env`

## Environment Variables

| Variable      | Description                   |
| ------------- | ----------------------------- |
| `CC_INFO`     | Color code: INFO (blue)       |
| `CC_WARN`     | Color code: WARN (yellow)     |
| `CC_ERROR`    | Color code: ERROR (magenta)   |
| `CC_CRITICAL` | Color code: CRITICAL (red)    |
| `CC_RESET`    | Color code: Use default again |

To use the color codes above use:

```bash
echo -e "${CC_INFO}Start deploying...${CC_RESET}"
```

The calculated environment variables are written to files, because they can not transporteed directly from one jopb to another. To use them you have to load them from file:

```yaml
script:
    - export $(cat ci-env/*.env | xargs)
```

!!! Attention

    This will only include from previous jobs, so check if the job is in your `needs` list.

The following environment variables are set:

| Variable            | Example                                  | Description                                             |
| ------------------- | ---------------------------------------- | ------------------------------------------------------- |
| `LAST_TAG`          | 'v0.6.3'                                 | Last tag in git which is considered as the current tag. |
| `NAMESPACE`         | 'divibib'                                | Namespae for deployment                                 |
| `LAST_PROD_SUCCESS` | ead06746e8f006ed5fefad9e01d9facce67f2fe6 | SHA of last successful prod pipeline.                   |

It will also generate a file called `init.changelog` which contains all the messages since last successfull production deployment.

## Database Setup

The initial SQL to create the `cid_run` record is addeed.
