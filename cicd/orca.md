# Orca Security Step (only AVGL)

This step will run Orca Security's scan for docker images in your pipeline.

## Setup

### Group setup

Set the variable: `ORCA_SECURITY_API_TOKEN` in variables section for group.
Also this will only run in **protected branches**.

### Project setup

Add the following include statement to your `.gitlab-ci.yml` file:

```yaml
include:
  - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/main.yml
  - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/cicd/orca.yml
```
