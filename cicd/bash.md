# Bash Application Tests

This will run shell tests using bats toolset.

It will call the tool to create a JUnit like report `junit.xml` file

## Jobs

| Job                    | Pipeline | Precondition                   | Tool                                      |
| ---------------------- | -------- | ------------------------------ | ----------------------------------------- |
| `unit-bats`            | branch   | `test/unit/bats/*.bats`        | [Bats](https://bats-core.readthedocs.io/) |
| `int-bats-debian-9`    | branch   | `test/integration/bats/*.bats` | [Bats](https://bats-core.readthedocs.io/) |
| `int-bats-debian-10`   | branch   | `test/integration/bats/*.bats` | [Bats](https://bats-core.readthedocs.io/) |
| `int-bats-debian-11`   | branch   | `test/integration/bats/*.bats` | [Bats](https://bats-core.readthedocs.io/) |
| `review-bats-test-rt`  | branch   | `test/review/bats/*.bats`      | [Bats](https://bats-core.readthedocs.io/) |
| `review-bats-test-ps`  | branch   | `test/review/bats/*.bats`      | [Bats](https://bats-core.readthedocs.io/) |
| `review-bats-int-rt`   | branch   | `test/review/bats/*.bats`      | [Bats](https://bats-core.readthedocs.io/) |
| `review-bats-int-ps`   | branch   | `test/review/bats/*.bats`      | [Bats](https://bats-core.readthedocs.io/) |
| `review-bats-stage-rt` | branch   | `test/review/bats/*.bats`      | [Bats](https://bats-core.readthedocs.io/) |
| `review-bats-stage-ps` | branch   | `test/review/bats/*.bats`      | [Bats](https://bats-core.readthedocs.io/) |
| `review-bats-prod-rt`  | branch   | `test/review/bats/*.bats`      | [Bats](https://bats-core.readthedocs.io/) |
| `review-bats-prod-ps`  | branch   | `test/review/bats/*.bats`      | [Bats](https://bats-core.readthedocs.io/) |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.
But the results will also be reported to the database store.

### Configuration

| Variable      | Default | Example                           | Description      |
| ------------- | ------- | --------------------------------- | ---------------- |
| `EXCLUDE_JOB` |         | `/unit/`, `/unit-bats/`, `/bats/` | Exclude this job |

## Example

It is automatically included using `main.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/main.yml
```

If you want to include only the parts you wish:

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/cicd/stages.yml
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/cicd/bash.yml
```

## Internal

| Variable       | Default | Description                                              |
| -------------- | ------- | -------------------------------------------------------- |
| `NODEJS_MAJOR` | 18      | Version to install on distribution for integration tests |