# Build Java app

This template will build the Java application using maven.

It will call `mvn test` and `mvn package verify`, `mvn site` and make the following folders available as artifact: `target/*.jar`, `target/*.war`, `target/site`.
Within the build it will also set the correct version and replaces environment variables in `pom.xml`.

## Jobs

| Job             | Pipeline | Precondition | Tool                         | Report Artifact      |
| --------------- | -------- | ------------ | ---------------------------- | -------------------- |
| `unit-maven`    | branch   | `pom.xml`    | [Mocha](https://mochajs.org) |                      |
| `build-maven`   | branch   | `pom.xml`    | [Mocha](https://mochajs.org) | \<target-dirctories> |
| `doc-maven`     | branch   | `pom.xml`    | [Mocha](https://mochajs.org) | ci-pages             |
| `release-maven` | branch   | `pom.xml`    | [Mocha](https://mochajs.org) | -                    |

### Configuration

| Variable        | Default | Example                              | Description                   |
| --------------- | ------- | ------------------------------------ | ----------------------------- |
| `EXCLUDE_JOB`   |         | `/build-maven/`, `/maven/`, `/unit/` | Exclude these jobs            |
| `JAVA_VERSION`  | `11`    |                                      | Java major version of OpenJDK |
| `MAVEN_VERSION` | `3`     | `3.8`                                | Maven major or minor version  |
| `MAVEN_PROFILE` |         | `ekz`                                | Profile to use for build      |

## Example

The job is added using the default templates:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/main.yml
```
