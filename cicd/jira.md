# Adding comment to JIRA Tickets

This will inform about a successfull production deployment on the depending Jira Tickets

## Jobs

Predefined Jobs:

| Job                 | Precondition | Format |
| ------------------- | ------------ | ------ |
| `jira-success-prod` | success      | HTML   |

### Configuration

| Variable      | Default | Example                    | Description      |
| ------------- | ------- | -------------------------- | ---------------- |
| `EXCLUDE_JOB` |         | `/jira/`, `/jira-success/` | Exclude this job |

## Example

It is automatically included using `main.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/main.yml
```
