# GitLab Pages

This template will deploy the documentation to GitLab pages making it available under `https://<username>.gitlab.io/<project path>`.

If a coverage report was created by the Unit Tests it will be included in the `coverage` subfolder.

It will also contain reports from tools, if there (in the subfolder):

-   `report/unit` - Unit Test Results
-   `report/coverage` - Coverage Report
-   `report/code-quality` - Code Quality

The `report` folder will also have an short index.

## Jobs

| Job            | Pipeline | Precondition | Tool   | Report Artifact |
| -------------- | -------- | ------------ | ------ | --------------- |
| `pages`        | Branch   |              | Bash   | public/         |
| `pages-deploy` | Branch   |              | Gitlab |                 |

To make it possible to cumulate reports over the different pipelines the `public` path is kept in cache.

The report will also push to the project root pages for protected push but for all other create a sub directory using the branch or tag name.

## Configuration

| Variable      | Default | example | Description      |
| ------------- | ------- | ------- | ---------------- |
| `EXCLUDE_JOB` |         | `pages` | Exclude this job |

## Example

```yaml title=".gitlab-ci.yml"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/cicd/pages.yml
```
