# Sending email

This template will define a hidden base job which will help you to send emails.

This will not include any job you have to define them yourself.

## Jobs

| Job                     | Pipeline      | Precondition | Tool | Report Artifact |
| ----------------------- | ------------- | ------------ | ---- | --------------- |
| `email-success-develop` | develop       | success      | Mail |                 |
| `email-success-main`    | main/master   | success      | Mail |                 |
| `email-success-prod`    | protected tag | success      | Mail |                 |
| `email-failure-develop` | develop       | failure      | Mail |                 |
| `email-failure-main`    | main/master   | failure      | Mail |                 |
| `email-failure-prod`    | protected tag | failure      | Mail |                 |

### Configuration

| Variable       | Default                       | Example                                   | Description         |
| -------------- | ----------------------------- | ----------------------------------------- | ------------------- |
| `EXCLUDE_JOB`  |                               | `email`, `email-success`, `email-failure` | Exclude this job    |
| `MAIL_TO`      | user which triggered pipeline | `betriebsteam@divibib.com`                | single mail address |
| `MAIL_SUBJECT` | `GitLab CI/CD Mail`           | `Test`                                    | subject of the mail |
| `MAIL_HTML`    | `<p>Hello, world!</p>`        | `<p>This is only a test</p>`              | html content        |

## Example

It is automatically included using `all.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/all.yml
```

### Own email job

It is automatically included using `all.yml` but you can load it yourself.

```yaml title=".gitlab-ci.yml" hl_lines="5 8 9 10 11"
include:
    - remote: "http://gitlab.service.office.grp/templates/gitlab-ci/raw/main/cicd/email.yml"

test email:
    extends: .email
    stage: test
    variables:
        MAIL_TO: betriebsteam@divibib.com
        MAIL_SUBJECT: Email 3
        MAIL_HTML: |
            <p>This is only a test</p>
```

As shown above you should extend the included `.email` template and set the variables.
