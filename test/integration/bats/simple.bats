#!/usr/bin/env bats

@test "call command: date" {
    run date
    echo "status = ${status}"
    echo "$output"
    [ "$status" = 0 ]
}
