describe('The website', function(){
    it('should display a button to submit an article on the homepage', function(){
        browser.url('/');

        expect(browser.isExisting('.hero a.is-primary')).toBe(true);
        expect(browser.element('.hero.is-primary a.is-primary').getText()).toBe('Sign in with ORCID');
    });

    it('should display an error message on non-existing pages', function(){
        browser.url('/page-that-does-not-exist');

        expect(browser.element('.hero h1.title').getText()).toBe('Page not found');
    });

    it('should be able to navigate to the homepage from the 404 page', function(){
        browser.url('/page-that-does-not-exist');
        expect(browser.getUrl()).toMatch('page-that-does-not-exist');

        browser.element('.content a[href="/"]').click();
        
        expect(browser.getUrl()).not.toMatch('page-that-does-not-exist');
    });
});
