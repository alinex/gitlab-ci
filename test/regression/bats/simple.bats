#!/usr/bin/env bats

@test "call command: hostname" {
    run ssh $SSH hostname -f
    echo "status = ${status}"
    echo "$output"
    [ "$status" = 0 ]
}
