# Bash Application Tests

This will shell tests on different Linux distributions. All distributions images will be getting support for bash and basic linux tools by installing appropriatre packages like `bash`, `curl`, `find`, `ncurses`, `tar` as well as the `nodejs` and `npm` tools for the Bats runner.

It will call the tool to create:

-   a JUnit like report `junit.xml` file per job

The following tests are supported:

| Tool                                      | Directory               | Files    | Jobs             |
| ----------------------------------------- | ----------------------- | -------- | ---------------- |
| [Bats](https://bats-core.readthedocs.io/) | `test/integration/bats` | `*.bats` | `bats-debian`    |
| [Bats](https://bats-core.readthedocs.io/) | `test/integration/bats` | `*.bats` | `bats-ubuntu`    |
| [Bats](https://bats-core.readthedocs.io/) | `test/integration/bats` | `*.bats` | `bats-mint`      |
| [Bats](https://bats-core.readthedocs.io/) | `test/integration/bats` | `*.bats` | `bats-redhat`    |
| [Bats](https://bats-core.readthedocs.io/) | `test/integration/bats` | `*.bats` | `bats-fedora`    |
| [Bats](https://bats-core.readthedocs.io/) | `test/integration/bats` | `*.bats` | `bats-centos`    |
| [Bats](https://bats-core.readthedocs.io/) | `test/integration/bats` | `*.bats` | `bats-mandrake`  |
| [Bats](https://bats-core.readthedocs.io/) | `test/integration/bats` | `*.bats` | `bats-archlinux` |
| [Bats](https://bats-core.readthedocs.io/) | `test/integration/bats` | `*.bats` | `bats-alpine`    |
| [Bats](https://bats-core.readthedocs.io/) | `test/integration/bats` | `*.bats` | `bats-opensuse`  |

It will take two runs, one with a visual reporter and a second one with a `junits.xml` export as both is not possible at the same time.

## Usage

It is automatically included using `all.yml` but you can load it yourself. The job will only be included if some tests under `test/integration/bats/*.bats` are found.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/integration/bash.yml
```

There are no variables used.
