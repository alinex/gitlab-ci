# gitlab-ci

Templates for GitLab CI/CD Tasks

This is a collection of different templates to make it easier to do CI/CD tasks. They are made for the alinex repositories, but you may also use them or create your own templates like these.

This templates use the following stages:

-   `test` - used for tests on the source code
-   `build` - building and packaging the code
-   `integration` - tests of the builded code
-   `deploy` - upload, deploy and activate on server
-   `review` - app on server: tests, performance

The last two stages may run on different environments like test, staging or production. Mostly the environment is decided based on the branch:

-   branch `develop` -> test
-   branch `main` -> staging
-   tag on `main` -> release to production

The templates are stored in folders per each stage to make selection easier. See the following overview of templates:

| Stage       | Auto | Template                               | Description                                 |
| ----------- | ---- | -------------------------------------- | ------------------------------------------- |
|             | X    | [stages.yml](stages.md)                | Define all possible stages                  |
| .pre        |      | [printenv.yml](printenv.md)            | Print all environment variables (debugging) |
| test        | X    | [nodejs.yml](test/nodejs.md)           | Unit tests with coverage report for NodeJS  |
| test        | X    | [bash.yml](test/bash.md)               | Bash unit tests                             |
| test        | X    | [secret.yml](test/secret.md)           | Secret detection                            |
| test        | X    | [dependency.yml](test/dependency.md)   | External dependency analysis                |
| test        | X    | [license.yml](test/license.md)         | License scanning                            |
| test        | X    | [quality.yml](test/quality.md)         | Code quality check                          |
| test        | X    | [sast.yml](test/sast.md)               | Static Application Security Testing         |
| build       | X    | [nodejs.yml](build/nodejs.md)          | Build NodeJS app                            |
| build       | X    | [docker.yml](build/docker.md)          | Build docker image                          |
| build       | X    | [mkdocs.yml](build/mkdocs.md)          | Create HTML documentation from markdown     |
| integration | X    | [bash.yml](integration/bash.md)        | Bash integration tests                      |
| deploy      | X    | [pages.yml](deploy/pages.md)           | Collect Documentation and reports to pages  |
| deploy      | X    | [upload-ssh.yml](deploy/upload-ssh.md) | Upload using SSH                            |
| deploy      | X    | [ssh.yml](deploy/ssh.md)               | Deploy using SSH                            |

In each stage and the root folder you also find an `all.yml` file which will include everything marked as auto above.

!!! Attention

    This is a special setup which works for the alinex modules. If you want to use it yourself you have to ensure that the structure is the same or make your own fork of it.

## Use template

At least the first two pre-conditions has to be fulfilled:

1. include: of the templates
2. stages: definition of which to run (optional with stages.yml)
3. rules: sometimes they specify based on branches, tags or content if it is used
4. variables: can be used to further customize

```yaml
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml
```

Sometimes they only include a template (job starting with `.`) which you have to extend but often there is also a running job beside it. But you can always overwrite the job and use the template with the same name to extend from and make your changes like `rules`, `needs`...

> If you use the global `all.yml` as above you will also get some dependencies between jobs of different stages which can improve overall runtime.

> Each template has it's documentation beside it, so please have a look at it.

The following VARIABLES are globally used for control:

| Variable      | Default | Example      | Description                                                  |
| ------------- | ------- | ------------ | ------------------------------------------------------------ |
| `EXCLUDE_JOB` |         | `dependency` | Space separated list of job names (or parts) to be excluded. |

For this to work the whole template structure has to be included using the `all.yml` above.

## Roadmap

-   [ ] https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml
