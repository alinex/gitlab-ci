# Build NodeJS app

This template will build the NodeJS application and make it available as artifact.

It will call `npm install` and `npm run build` and make the following changed folders available as artifact: `bin`, `lib`, `node_modules`

It will run after all tests are done.

| Tool | Jobs           |
| ---- | -------------- |
| NPM  | `build-nodejs` |

## Usage

It is automatically included using `all.yml` but you can load it yourself. The job will only be included if a `package.json` file is available or you added it by yourself.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/build/nodejs.yml

variables:
    NODEJS_VERSION: 12
```

As shown above you can specify something using variables:

| Variable         | Default  | Example | Description                  |
| ---------------- | -------- | ------- | ---------------------------- |
| `NODEJS_VERSION` | `latest` | `12`    | version number for build run |

It already has a default job which you can overwrite if you have to make more changes as changing the artifact folders:

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/build/nodejs.yml

variables:
    NODEJS_VERSION: 12

build-nodejs:
  extends: .build-nodejs
  artifacts:
    paths:
      - !reference [.build-nodejs, artifacts, paths]
      - myfolder
```

This will also include the `myfolder` folder.
