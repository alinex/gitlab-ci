# Build Docker image using kaniko

This template will build the docker image

The following jobs are supported:

| Tool   | Jobs           |
| ------ | -------------- |
| Docker | `build-docker` |

## Usage

You need to include the template and define the variables.

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/build/docker.yml
```

It will add a job called `build_docker` but only run if you have

-   set the variables below
-   and added a semver tag
-   and a `/Dockerfile` exists

You need to specify the registry within the project (Settings > CI/CD > Variables):

| Variable            | Example                       | Description            |
| ------------------- | ----------------------------- | ---------------------- |
| `REGISTRY`          | `https://index.docker.io/v1/` | The repository API URL |
| `REGISTRY_IMAGE`    | `docker.io/alinex/mkdocs`     | Path for the image     |
| `REGISTRY_USER`     | `alinex`                      | Username               |
| `REGISTRY_PASSWORD` | `xxxxxxxxxxxxxxxxx`           | Password               |

You can set them all within the project to also mask the password.

If you want to specify your own `rules` or `needs` you can do it by define a job like:

```yaml
build-docker:
    extends: .build-docker
```
