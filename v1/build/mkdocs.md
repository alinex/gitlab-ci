# Build HTML documentation from markdown using MkDocs

This template will build the documentation out of markdown files using MkDocs.

The artifact will contain

-   the folder `build-docs` with all files

It will run immediately because it do not need other data.

The following jobs are supported:

| Tool   | Jobs     |
| ------ | -------- |
| MkDocs | `mkdocs` |

## Usage

In most cases it is enough to simply include it:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/build/mkdocs.yml
```

It will look for a `mkdocs.yml` file and add a job if found.

You can further configure it using some variables:

| Variable          | Default     | Example | Description                                   |
| ----------------- | ----------- | ------- | --------------------------------------------- |
| `PAGES_BRANCH` | `<default>` | `main`  | run only on the defined branch                |
| `MKDOCS_DIR`   | `site`      | `site`  | specify directory in which mkdocs will build. |

If you want to specify your own `rules` or `needs` you can do it by define a job like:

```yaml
mkdocs:
    extends: .mkdocs
```
