# GitLab Pages

This template will deploy the documentation to GitLab pages making it available under `https://<username>.gitlab.io/<project path>`.

If a coverage report was created by the Unit Tests it will be included in the `coverage` subfolder.

It will also contain reports from tools, if there (in the subfolder):

-   `report/unit` - Unit Test Results
-   `report/coverage` - Coverage Report
-   `report/code-quality` - Code Quality

The `report` folder will also have an short index.

It will use artifacts from:

-   [test/nodejs](../test/nodejs.md)
-   [test/quality](../test/quality.md)
-   [build/mkdocs](../build/mkdocs.md)

The following jobs are supported:

| Tool   | Jobs           |
| ------ | -------------- |
| Bash   | `pages`        |
| Gitlab | `pages-deploy` |

## Usage

In most cases it is enough to simply include it:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/deploy/pages.yml
```

It will look for a `mkdocs.yml` file and add a `pages` job if found.

If you want to specify your own `rules` or `needs` you can do it by define a job like:

```yaml
pages:
    extends: .pages
```
