# SSH Upload

This template will only build a base for ssh commands to deploy on the server using a SSH connection. You have to define the real work yourself.

It has three templates to be used:

-   `.ssh` - basic ssh action
-   `.upload-ssh` - base for upload
-   `.deploy-ssh` - base for deploy

The following jobs are supported:

| Trigger            | Job              | Description            |
| ------------------ | ---------------- | ---------------------- |
| develop branch     | `upload-develop` | upload to test         |
| develop branch     | `deploy-develop` | deploy to test         |
| main/master branch | `upload-main`    | upload to staging/test |
| main/master branch | `deploy-main`    | deploy to staging/test |
| main/master tag    | `upload-prod`    | upload to production   |
| main/master tag    | `deploy-prod`    | deploy to production   |

## Upload

The upload will cleanup old deployments and copy the new files onto the server.

Within the given `UPLOAD_FOLDER` a directory named `<project>-<tag>-<pipeline-id>` will be created. It also includes a `version.info` file.

```text title="version.info"
NAME        betrieb/operations
REVISION    1b692ea535794e79449f8cef029948a933aea7c8
TAG         v1.9.0
LANGUAGES   typescript,shell,javascript,vim script
PIPELINE    https://gitlab.com/alinex/operations/-/pipelines/4728
USER        alinex <info@alinex.de>
DATE        2021-12-30T14:35:08+01:00
PROJECT     https://gitlab.com/alinex/operations
REPOSITORY  http://gitlab-ci-token:[MASKED]@gitlab.com/alinex/operations.git
```

## Deploy

Within the deployment the following steps will be done:

-   check if directory is still there
-   copy directory
-   replace current with new version
-   write install log

While within a freeze time (set in Settings > CI/CD > Deploy freezes) the production deploy will be not made automatically and you have to do this manually.

## Usage

It is automatically included using `all.yml` but you can load it yourself. This job only contains a template so you need to define your own implementation.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml

variables:
    UPLOAD_PARTS: actor bin config data lib node_modules
```

As shown above you need the same variables as used for the upload:

| Variable                | Default                            | Example                               | Description                                           |
| ----------------------- | ---------------------------------- | ------------------------------------- | ----------------------------------------------------- |
| `SSH_PRIVATE_KEY`       |                                    | `----BEGIN RSA PRIVATE KEY-----\n...` | The private ssh key to use for connections (mask it). |
| `UPLOAD_PARTS`          | `bin config data lib node_modules` |                                       | Directories or files to upload                        |
| `UPLOAD_KEEP_OLD`       | 3                                  |                                       | Number of old uploads to keep.                        |
| `DEPLOY_WAIT`           | 20                                 |                                       | Seconds to wait after each deploy.                    |
| `UPLOAD_FOLDER`         | `/opt/divibib/upload`              |                                       | Folder the code will be uploaded to.                  |
| `PROD_UPLOAD_SSH`       |                                    | `admin@operations.host.test.dvb`      | SSH connection string like `user@host.office.dvb`.    |
| `PROD_UPLOAD_FOLDER`    | `/opt/divibib/upload`              |                                       | Folder the code will be uploaded to.                  |
| `PROD_DEPLOY_PATH`      | `/opt/divibib/<project-name>`      |                                       | Path there the application will be deployed.          |
| `MAIN_UPLOAD_SSH`       |                                    | `admin@operations.host.staging.dvb`   | SSH connection string like `user@host.office.dvb`.    |
| `MAIN_UPLOAD_FOLDER`    | `/opt/divibib/upload`              |                                       | Folder the code will be uploaded to.                  |
| `MAIN_DEPLOY_PATH`      | `/opt/divibib/<project-name>`      |                                       | Path there the application will be deployed.          |
| `DEVELOP_UPLOAD_SSH`    |                                    | `admin@operations.host.cloud.dvb`     | SSH connection string like `user@host.office.dvb`.    |
| `DEVELOP_UPLOAD_FOLDER` | `/opt/divibib/upload`              |                                       | Folder the code will be uploaded to.                  |
| `DEVELOP_DEPLOY_PATH`   | `/opt/divibib/<project-name>`      |                                       | Path there the application will be deployed.          |

All Settings with no default value need to be provided if the specific jobs should run.
You can overwrite `rules`, `needs`, `after_script` and need to set the `script` with the real details. To change the `before_script` use `!reference` syntax to extend it.

### Individual upload/deploy

```yaml title=".gitlab-ci.yml"
upload-self:
    extends: .upload-ssh
    variables:
        UPLOAD_SSH: $PROD_UPLOAD_SSH
        UPLOAD_FOLDER: $PROD_UPLOAD_FOLDER
    rules:
        - if: $PROD_UPLOAD_SSH == ""
          when: never
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_TAG

deploy-self:
    extends: .deploy-ssh
    variables:
        UPLOAD_SSH: $PROD_UPLOAD_SSH
        UPLOAD_FOLDER: $PROD_UPLOAD_FOLDER
        DEPLOY_PATH: $PROD_DEPLOY_PATH
    needs:
        - job: upload-self
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_TAG
```

### Own scripts

```yaml title=".gitlab-ci.yml"
deploy:
    extends: .ssh
    needs:
        - job: build-nodejs
          artifacts: true
          optional: true
    script:
        -  # run your deploy script here
    rules:
        - if: $PROD_UPLOAD_SSH == ""
          when: never
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_TAG
```
