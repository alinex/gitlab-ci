# Dependency scanning

This template will run dependency scanning for different languages and create reports.
It will create a report named `dependency_scanning` as artifact containing `gl-dependency-scanning-report.json`.

!!! note

    See also the [GitLab documentation](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/).

These results will be visible in:

-   The merge request dependency scanning widget.
-   The pipeline Security tab.
-   The security dashboard: "Security & Compliance" > "Security dashboard"
-   The Project Vulnerability report: "Security & Compliance" > "Vulnerability report"
-   The dependency list: "Security & Compliance" > "Dependency list"

This may be done in multiple jobs:

| Tool      | Files                                                                                                                                                 | Jobs                   |
| --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- |
| gemnasium | `Gemfile.lock`, `composer.lock`, `gems.locked`, `go.sum`, `npm-shrinkwrap.json`, `package-lock.json`, `yarn.lock`, `packages.lock.json`, `conan.lock` | `dependency-gemnasium` |
| maven     | `build.gradle`, `build.gradle.kts`, `build.sbt`, `pom.xml`                                                                                            | `dependency-maven`     |
| python    | `Gemfile.lock`                                                                                                                                        | `dependency-python`    |
| bundler   | `Gemfile.lock`                                                                                                                                        | `dependency-bundler`   |
| retirejs  | `package.json`                                                                                                                                        | `dependency-retirejs`  |

## Usage

In most cases it is enough to simply include it:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/test/dependency.yml
```

The following analyzers will be used:

| Name                 | When                                                                                                                                                  |
| -------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- |
| dependency-gemnasium | `Gemfile.lock`, `composer.lock`, `gems.locked`, `go.sum`, `npm-shrinkwrap.json`, `package-lock.json`, `yarn.lock`, `packages.lock.json`, `conan.lock` |
| dependency-maven     | `build.gradle`, `build.gradle.kts`, `build.sbt`, `pom.xml`                                                                                            |
| dependency-python    | `requirements.txt`, `requirements.pip`, `Pipfile`, `requires.txt`, `setup.py`                                                                         |
| dependency-bundler   | `Gemfile.lock`                                                                                                                                        |
| dependency-retirejs  | `package.json`                                                                                                                                        |

If you want to specify your own `rules` or `needs` you can do it by rewrite a job like:

```yaml
dependency-gemnasium:
    extends: .dependency-gemnasium
```
