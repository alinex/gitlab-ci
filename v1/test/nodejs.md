# Unit Test NodeJS app

This will run Unit Tests on a nodejs application.
Support for multiple tools is added here.

It will call the tool to create:

-   a JUnit like report `junit.xml` file
-   a coverage summary report `cobertura-coverage.xml` file
-   an archive with `coverage/` output including HTML files

The following jobs are supported:

| Tool  | Directory    | Files  | Jobs       |
| ----- | ------------ | ------ | ---------- |
| Mocha | `test/mocha` | `*.js` | `mocha-js` |
| Mocha | `test/mocha` | `*.ts` | `mocha-ts` |

The coverage results will be visualized inside the file diff view of your merge request and in the HTML files which will be made available in the pages subfolder `report/coverage` by the pages creation job.

## Usage

It is automatically included using `all.yml` but you can load it yourself. The job will only be included if the tool specific test cases are present under `test/<tool>`.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/test/nodejs.yml

variables:
    AX_NODEJS_VERSION: 12
```

As shown above you can specify something using variables:

| Variable            | Default  | Example | Description                  |
| ------------------- | -------- | ------- | ---------------------------- |
| `AX_NODEJS_VERSION` | `latest` | `12`    | version number for build run |
