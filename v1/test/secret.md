# Secret Detection

The secret-detection report collects detected secrets. The collected Secret Detection report is uploaded to GitLab.
It will create a report named `secret_detection` as artifact containing `gl-secret-detection-report.json`.

!!! note

    See also the [GitLab documentation](https://docs.gitlab.com/ee/user/application_security/secret_detection/).

Secrets include keys, passwords, API tokens, and other sensitive information.
Secret Detection uses the Gitleaks tool to scan the repository for secrets.

GitLab can display the results of one or more reports in:

-   The merge request secret scanning widget.
-   The pipeline Security tab.
-   The security dashboard: "Security & Compliance" > "Security Dashboard"

The following jobs are supported:

| Tool      | Jobs     |
| --------- | -------- |
| Wikileaks | `secret` |

## Usage

In most cases it is enough to simply include it:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/test/secret.yml
```

If you want to specify your own `rules` or `needs` you can do it by rewrite a job like:

```yaml
secret:
    extends: .secret
```
