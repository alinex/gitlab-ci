# Bash Application Tests

This will shell tests.

It will call the tool to create:

-   a JUnit like report `junit.xml` file

The following tests are supported:

| Tool                                      | Directory   | Files    | Jobs   |
| ----------------------------------------- | ----------- | -------- | ------ |
| [Bats](https://bats-core.readthedocs.io/) | `test/bats` | `*.bats` | `bats` |

It will sometimes take two runs, one with a visual reporter and a second one with a `junits.xml` export as both is not possible at the same time.

## Usage

It is automatically included using `all.yml` but you can load it yourself. The job will only be included if some tests under `test/bats/*.bats` are found.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/integration/bash.yml
```

There are no variables used.
