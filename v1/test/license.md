# License compliance

License Compliance to search your project’s dependencies for their licenses.
It will create a report named `license_scanning` as artifact containing `gl-license-scanning-report.json`.

!!! note

    See also the [GitLab documentation](https://docs.gitlab.com/ee/user/compliance/license_compliance/).

GitLab can display the results of one or more reports in:

-   The merge request license compliance widget.
-   The license list: "Security & Compliance" > "License compliance"

The following jobs are supported:

| Tool    | Jobs      |
| ------- | --------- |
| license | `license` |

## Usage

In most cases it is enough to simply include it:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/test/license.yml
```

If you want to specify your own `rules` or `needs` you can do it by rewrite a job like:

```yaml
license:
    extends: .license
```
