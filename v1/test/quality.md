# Code quality checks

At the moment this uses the default checks from GitLab.

It will create:

-   a codequality report with `gl-code-quality-report.json` file
-   an archive with `gl-code-quality-report.html` file

This is done using Code Climate plugins and runs in two jobs.

GitLab can display the results of one or more reports in:

-   The merge request license compliance widget.
-   if pages are used in the `report/code-quality` folder

The following jobs are supported:

| Tool        | Jobs           |
| ----------- | -------------- |
| CodeClimate | `code-quality` |

!!! note

    See also the [GitLab documentation](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html).

## Usage

It is automatically included using `all.yml` but you can load it yourself. The job will only be included if a `package.json` file is available or you added it by yourself.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/test/quality.yml
```
