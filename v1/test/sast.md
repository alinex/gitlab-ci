# Static Application Security Testing

This will check the source code for known vulnerabilities.

The results are sorted by the priority of the vulnerability: Critical, High, Medium, Low, Info, Unknown

The collected SAST report uploads to GitLab as an artifact:

-   report with `gl-sast-report.json` file

This can be shown:

-   in the merge request SAST widget
-   in the security dashboard
-   in the vulnerability report

The following jobs are supported:

| Tool                 | Files                                                   | Jobs                        |
| -------------------- | ------------------------------------------------------- | --------------------------- |
| bandit               | `*.py`                                                  | `sast-bandit`               |
| brakeman             | `*.rb`, `Gemfile`                                       | `sast-brakeman`             |
| eslint               | `*.html`, `*.js`, `*.jsx`, `*.ts`, `*.tsx`              | `sast-eslint`               |
| flawfinder           | `*.c`, `*.cpp`                                          | `sast-flawfinder`           |
| kubesec              | `$SCAN_KUBERNETES_MANIFESTS == 'true'`                  | `sast-kubesec`              |
| gosec                | `*.go`                                                  | `sast-gosec`                |
| mobsf-android        | `*.apk`, `AndroidManifest.xml`                          | `sast-mobsf-android`        |
| mobsf-ios            | `*.ipa`, `*.xcodeproj/`                                 | `sast-mobsf-ios`            |
| nodejs-scan          | `package.json`                                          | `sast-nodejs-scan-`         |
| phpcs-security-audit | `*.php`                                                 | `sast-phpcs-security-audit` |
| pmd-apex             | `*.cls`                                                 | `sast-pmd-apex`             |
| security-code-scan   | `*.csproj`, `*.vbproj`                                  | `sast-security-code-scan`   |
| semgrep              | `*.py`, `*.js`, `*.jsx`, `*.ts`, `*.tsx`, `*.c`, `*.go` | `sast-semgrep`              |
| sobelow              | `mix.exs`                                               | `sast-sobelow`              |
| spotbugs             | `*.groovy`, `*.java`, `*.scala`, `*.kt`                 | `sast-spotbugs`             |

!!! note

    See also the [GitLab documentation](https://docs.gitlab.com/ee/user/application_security/sast/).

## Usage

It is automatically included using `all.yml` but you can load it yourself. The job will only be included if a `package.json` file is available or you added it by yourself.

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/test/sast.yml
```
