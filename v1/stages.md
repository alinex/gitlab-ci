# Default Stages

This template will define all the default stages:

-   `test` - used for tests on the source code
-   `build` - building and packaging the code
-   `integration` - tests of the builded code
-   `deploy` - upload, deploy and activate on server
-   `review` - app on server: tests, performance

The last two stages may run on different environments like test, staging or production. Mostly the environment is decided based on the branch:

-   branch `develop` -> test
-   branch `main` -> staging
-   tag on `main` -> release to production

## Usage

It is automatically included using `all.yml` but you can load it yourself.

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/stages.yml
```

It has nothing to configure.
