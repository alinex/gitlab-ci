# Print all environment variables

This is helpful to write your CI Scripts. 

It will print all environment variables set.


## Usage

This is **not** automatically included using `all.yml` and has to be added if needed by yourself.

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/printenv.yml
```

It has nothing to configure.
