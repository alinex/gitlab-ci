# Build Docker image using kaniko

This template will build the docker image

## Jobs

| Job            | Pipeline               | Precondition | Tool                                                     | Report Artifact |
| -------------- | ---------------------- | ------------ | -------------------------------------------------------- | --------------- |
| `build-docker` | branch / protected tag | Dockerfile   | [kaniko](https://github.com/GoogleContainerTools/kaniko) | -               |

### Configuration

| Variable            | Default | Example                           | Description              |
| ------------------- | ------- | --------------------------------- | ------------------------ |
| `EXCLUDE_JOB`       |         | `build`, `build-docker`, `docker` | Exclude this job         |
| `REGISTRY`          |         | `https://index.docker.io/v1/`     | The repository API URL   |
| `REGISTRY_IMAGE`    |         | `docker.io/alinex/mkdocs`         | Path for the image       |
| `REGISTRY_USER`     |         | `alinex`                          | Username                 |
| `REGISTRY_PASSWORD` |         | `xxxxxxxxxxxxxxxxx`               | Password or Access Token |

## Example

The job is added using the default templates:

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml
```

The variables can be set within the GiLab UI, too. But keep in mind to mask the password.
