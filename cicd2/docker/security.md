# Docker security check

This is not working at the moment and therefore disabled by rules!

The docker image OS, filesystem and package managers will be checked against vulnerabilities.

## Jobs

| Job               | Pipeline               | Precondition | Tool                                           | Report Artifact      |
| ----------------- | ---------------------- | ------------ | ---------------------------------------------- | -------------------- |
| `security-docker` | branch / protected tag | `Dockerfile` | [trivy](https://github.com/aquasecurity/trivy) | security-docker.json |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

### Configuration

| Variable            | Default | Example                                 | Description              |
| ------------------- | ------- | --------------------------------------- | ------------------------ |
| `INCLUDE_JOB`       |         | `security`, `security-docker`, `docker` | Exclude this job         |
| `REGISTRY`          |         | `https://index.docker.io/v1/`           | The repository API URL   |
| `REGISTRY_IMAGE`    |         | `docker.io/alinex/mkdocs`               | Path for the image       |
| `REGISTRY_USER`     |         | `alinex`                                | Username                 |
| `REGISTRY_PASSWORD` |         | `xxxxxxxxxxxxxxxxx`                     | Password or Access Token |

## Example

It is automatically included using `all.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml
```
