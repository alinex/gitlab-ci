# Push Docker image to registry

This template will build the docker image

The following jobs are supported:

## Jobs

| Job           | Pipeline | Precondition | Tool                                                                                     | Report Artifact |
| ------------- | -------- | ------------ | ---------------------------------------------------------------------------------------- | --------------- |
| `push-docker` | branch   | `Dockerfile` | [Crane](https://github.com/google/go-containerregistry/blob/main/cmd/crane/doc/crane.md) | -               |

### Configuration

| Variable            | Default | Example                         | Description              |
| ------------------- | ------- | ------------------------------- | ------------------------ |
| `INCLUDE_JOB`       |         | `push`, `push-docker`, `docker` | Exclude this job         |
| `REGISTRY`          |         | `https://index.docker.io/v1/`   | The repository API URL   |
| `REGISTRY_IMAGE`    |         | `docker.io/alinex/mkdocs`       | Path for the image       |
| `REGISTRY_USER`     |         | `alinex`                        | Username                 |
| `REGISTRY_PASSWORD` |         | `xxxxxxxxxxxxxxxxx`             | Password or Access Token |

## Example

It is automatically included using `all.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml
```
