# Build NodeJS app

This template will build the NodeJS application and make it available as artifact.

It will call `npm install` and `npm run build` and make the following folders available as artifact: `bin`, `config`, `data`, `lib`, `node_modules`

It will run after all tests are done.

## Jobs

| Job            | Pipeline               | Precondition            | Tool | Report Artifact |
| -------------- | ---------------------- | ----------------------- | ---- | --------------- |
| `nodejs-build` | branch / protected tag | `test/unit/bats/*.bats` | npm  | -               |

### Configuration

| Variable         | Default  | Example                 | Description                  |
| ---------------- | -------- | ----------------------- | ---------------------------- |
| `EXCLUDE_JOB`    |          | `build`, `build-nodejs` | Exclude this job             |
| `NODEJS_VERSION` | `latest` | `12`                    | version number for build run |

## Example

The job is added using the default templates:

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml

variables:
    NODEJS_VERSION: 12
```
