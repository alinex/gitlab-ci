# Unit Test NodeJS app

This will run if unit tests are found for a NodeJS application.
Support for multiple tools is added here.

## Jobs

| Job                  | Pipeline               | Precondition           | Tool                         | Report Artifact                       |
| -------------------- | ---------------------- | ---------------------- | ---------------------------- | ------------------------------------- |
| `node-unit-mocha-js` | branch / protected tag | `test/unit/mocha/*.js` | [Mocha](https://mochajs.org) | mocha-js.xml, mocha-js.html, coverage |
| `node-unit-mocha-ts` | branch / protected tag | `test/unit/mocha/*.ts` | [Mocha](https://mochajs.org) | mocha-ts.xml, mocha-ts.html, coverage |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

### Configuration

| Variable         | Default  | Example                             | Description                  |
| ---------------- | -------- | ----------------------------------- | ---------------------------- |
| `EXCLUDE_JOB`    |          | `node`, `unit`, `mocha`, `mocha-js` | Exclude this job             |
| `NODEJS_VERSION` | `latest` | `12`                                | version number for build run |

## Example

It is automatically included using `all.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml

variables:
    NODEJS_VERSION: 12
```

If you want to include only the parts you wish:

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/cicd/stages.yml
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/cicd/node/unit.yml

variables:
    NODEJS_VERSION: 12
```
