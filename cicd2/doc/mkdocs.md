# Build HTML documentation from source

This template will build the documentation out of markdown files using MkDocs.

The artifact will contain

-   the folder `build-docs` with all files

It will run immediately because it do not need other data.

## Jobs

| Job           | Pipeline               | Precondition | Tool                              | Report Artifact |
| ------------- | ---------------------- | ------------ | --------------------------------- | --------------- |
| `docs-mkdocs` | branch / protected tag | mkdocs.yml   | [MkDocs](https://www.mkdocs.org/) | `build-docs/`   |

### Configuration

| Variable       | Default     | Example               | Description                                   |
| -------------- | ----------- | --------------------- | --------------------------------------------- |
| `EXCLUDE_JOB`  |             | `docs`, `docs-mkdocs` | Exclude this job                              |
| `PAGES_BRANCH` | `<default>` | `main`                | run only on the defined branch                |
| `MKDOCS_DIR`   | `site`      | `site`                | specify directory in which mkdocs will build. |

## Example

The job is added using the default templates:

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml
```
