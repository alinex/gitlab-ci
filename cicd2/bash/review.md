# Bash Application Tests

This will run shell tests for the deployed application.

It will call the tool to create a JUnit like report `junit.xml` file

## Jobs

| Job                       | Pipeline      | Precondition                  | Tool                                      | Report Artifact                                           |
| ------------------------- | ------------- | ----------------------------- | ----------------------------------------- | --------------------------------------------------------- |
| `regression-bats-develop` | develop       | `test/regression/bats/*.bats` | [Bats](https://bats-core.readthedocs.io/) | regression-bats-develop.xml, regression-bats-develop.html |
| `regression-bats-main`    | main/master   | `test/regression/bats/*.bats` | [Bats](https://bats-core.readthedocs.io/) | regression-bats-main.xml, regression-bats-main.html       |
| `regression-bats-prod`    | protected tag | `test/regression/bats/*.bats` | [Bats](https://bats-core.readthedocs.io/) | regression-bats-prod.xml, regression-bats-prod.html       |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

### Configuration

| Variable                | Default                       | Example                                 | Description                                  |
| ----------------------- | ----------------------------- | --------------------------------------- | -------------------------------------------- |
| `EXCLUDE_JOB`           |                               | `regression`, `regression-bats`, `bats` | Exclude this job                             |
| `PROD_UPLOAD_SSH`       |                               | `admin@operations.host.test.dvb`        | SSH connection string list                   |
| `PROD_DEPLOY_PATH`      | `/opt/divibib/<project-name>` |                                         | Path there the application will be deployed. |
| `MAIN_UPLOAD_SSH`       |                               | `admin@operations.host.staging.dvb`     | SSH connection string list                   |
| `MAIN_UPLOAD_FOLDER`    | `/opt/divibib/upload`         |                                         | Folder the code will be uploaded to.         |
| `DEVELOP_UPLOAD_SSH`    |                               | `admin@operations.host.test.dvb`        | SSH connection string list                   |
| `DEVELOP_UPLOAD_FOLDER` | `/opt/divibib/upload`         |                                         | Folder the code will be uploaded to.         |

## Example

It is automatically included using `all.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/all.yml
```

## Test Scripts

The test scripts (located under `test/review/bats`) will get the following variables to connect to the server:

| Variable      | Example                          | Description                               |
| ------------- | -------------------------------- | ----------------------------------------- |
| `SSH`         | `admin@operations.host.test.dvb` | SSH connection string (multiple possible) |
| `DEPLOY_PATH` | `/opt/divibib/operator`          | Base path of the deployed application     |
| `NODE`        | `operations`                     | Shortname of the host                     |

You can use this to call a command on the remote host:

```bash
ssh $SSH hostname -f
```

This will get the hostname from the remote host.
