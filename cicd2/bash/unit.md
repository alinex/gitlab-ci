# Bash Application Tests

This will run shell tests using bats toolset.

It will call the tool to create a JUnit like report `junit.xml` file

## Jobs

| Job         | Pipeline               | Precondition            | Tool                                      | Report Artifact     |
| ----------- | ---------------------- | ----------------------- | ----------------------------------------- | ------------------- |
| `unit-bats` | branch / protected tag | `test/unit/bats/*.bats` | [Bats](https://bats-core.readthedocs.io/) | bats.xml, bats.html |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

### Configuration

| Variable      | Default | Example                     | Description      |
| ------------- | ------- | --------------------------- | ---------------- |
| `EXCLUDE_JOB` |         | `unit`, `unit-bats`, `bats` | Exclude this job |

## Example

It is automatically included using `all.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml
```

If you want to include only the parts you wish:

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/cicd/stages.yml
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/cicd/test/bats.yml
```
