# Bash Application Tests

This will shell tests on different Linux distributions. All distributions images will be getting support for bash and basic linux tools by installing appropriatre packages like `bash`, `curl`, `find`, `ncurses`, `tar` as well as the `nodejs` and `npm` tools for the Bats runner.

It will call the tool to create a JUnit like report `junit.xml` file

## Jobs

| Job                  | Pipeline | Precondition                   | Tool                                      | Report Artifact                 |
| -------------------- | -------- | ------------------------------ | ----------------------------------------- | ------------------------------- |
| `int-bats-debian-9`  | branch   | `test/integration/bats/*.bats` | [Bats](https://bats-core.readthedocs.io/) | int-bats-_.xml, int-bats-_.html |
| `int-bats-debian-10` | branch   | `test/integration/bats/*.bats` | [Bats](https://bats-core.readthedocs.io/) | int-bats-_.xml, int-bats-_.html |
| `int-bats-debian-11` | branch   | `test/integration/bats/*.bats` | [Bats](https://bats-core.readthedocs.io/) | int-bats-_.xml, int-bats-_.html |

The distribution will have NodeJS installed in the version specified under `$NODEJS_VERSION` or the latest one (defined in template).

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

### Configuration

| Variable      | Default | Example                   | Description      |
| ------------- | ------- | ------------------------- | ---------------- |
| `EXCLUDE_JOB` |         | `int`, `int-bats`, `bats` | Exclude this job |

## Example

It is automatically included using `all.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml
```

################################

It will call the tool to create:

-   a JUnit like report `junit.xml` file per job

The following tests are supported:

All of them will use [Bats](https://bats-core.readthedocs.io/) and work on `test/integration/bats/*.bats` files.

## Usage

It is automatically included using `all.yml` but you can load it yourself. The job will only be included if some tests under `test/integration/bats/*.bats` are found.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/cicd/integration/bash.yml
```

The following variables are supported:

| Variable         | Default | Example | Description                      |
| ---------------- | ------- | ------- | -------------------------------- |
| `NODEJS_VERSION` | 16      |         | The NOdeJS major version to use. |
