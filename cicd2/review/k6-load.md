# k6 Regression/Load Test

This will run the [k6](https://k6.io/blog/integrating-load-testing-with-gitlab/) test under:

-   `test/load/k6/index.js`

## Jobs

| Job                 | Pipeline      | Precondition              | Tool                 | Report Artifact      |
| ------------------- | ------------- | ------------------------- | -------------------- | -------------------- |
| `load-bats-develop` | develop       | `test/load/k6/index.json` | [k6](https://k6.io/) | load-k6-develop.json |
| `load-bats-main`    | main/master   | `test/load/k6/index.json` | [k6](https://k6.io/) | load-k6-main.json    |
| `load-bats-prod`    | protected tag | `test/load/k6/index.json` | [k6](https://k6.io/) | load-k6-prod.json    |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

### Configuration

| Variable             | Default | Example                                   | Description              |
| -------------------- | ------- | ----------------------------------------- | ------------------------ |
| `EXCLUDE_JOB`        |         | `k6`, `load`, `load-k6`                   | Exclude this job         |
| `PROD_UPLOAD_URL`    |         | `http://operations.host.test.dvb:4000`    | URL list to run tests on |
| `MAIN_UPLOAD_URL`    |         | `http://operations.host.staging.dvb:4000` | URL list to run tests on |
| `DEVELOP_UPLOAD_URL` |         | `http://operations.host.test.dvb:4000`    | URL list to run tests on |

## Example

It is automatically included using `all.yml` but you can load it yourself, too. The job will only be included if the tool specific test cases are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/all.yml
```

## Test Scripts

The test scripts should then get the selected URL:

| Variable | Example                                                                  | Description    |
| -------- | ------------------------------------------------------------------------ | -------------- |
| `URL`    | `[admin@operations.host.test.dvb](http://operations.host.test.dvb:4000)` | URL to test on |

A simple GET request should therefore look like:

```bash
import { sleep } from 'k6';
import http from 'k6/http';

export let options = {
  duration: '1m',
  vus: 50,
  thresholds: {
    http_req_duration: ['p(95)<500'], // 95 percent of response times must be below 500ms
  },
};

export default function () {
  http.get(`${__ENV.URL}`);
  sleep(3);
}
```
