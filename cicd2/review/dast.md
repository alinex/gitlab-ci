# Dynamic Application Security Testing

This will execute [ZAP’s Baseline Scan](https://www.zaproxy.org/docs/docker/baseline-scan/) and doesn’t actively attack your application.

But it can be configured to also perform an active scan to attack your application and produce a more extensive security report.

It will use the url from the specific server variables.

## Jobs

| Job            | Pipeline      | Precondition | Tool | Report Artifact   |
| -------------- | ------------- | ------------ | ---- | ----------------- |
| `dast-develop` | develop       |              |      | dast-develop.json |
| `dast-main`    | main/master   |              |      | dast-main.json    |
| `dast-prod`    | protected tag |              |      | dast-prod.json    |

### Configuration

| Variable                 | Default   | Example                                         | Description                                         |
| ------------------------ | --------- | ----------------------------------------------- | --------------------------------------------------- |
| `INCLUDE_JOB`            |           | `dast`, `dast-develop`...                       | Include these jobs                                  |
| `PROD_URL`               |           | `http.//operations.host.test.dvb:4000`          | URL to be called                                    |
| `MAIN_URL`               |           | `http.//operations.host.staging.dvb:4000`       | URL to be called                                    |
| `DEVELOP_URL`            |           | `http.//operations.host.test.dvb:4000`          | URL to be called                                    |
| `DAST_FULL_SCAN_ENABLED` | `true`    | `false`                                         | Disable active scanning                             |
| `DAST_API_OPENAPI`       |           | `http://my.api/api-specification.yml`           | OpenAPI Specification                               |
| `DAST_PATHS`             |           | `/page1.html,/category1/page1.html,/page3.html` | List of paths to check                              |
| `DAST_EXCLUDE_RULES`     |           | `10038,10055`                                   | Disables the rules with the given IDs.              |
| `DAST_SPIDER_MINS`       | unlimited | `1`                                             | The maximum duration of the spider scan in minutes. |

See the full [configuration description](https://docs.gitlab.com/ee/user/application_security/dast/#full-scan).

## Example

By default this job will not run, you have to activate it using `INCLUDE_JOB`:

```yaml title=".gitlab-ci.yml"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/all.yml

variables:
    INCLUDE_JOB: dast
```
