# Code quality checks

At the moment this uses the default checks from GitLab.

It will create:

-   a codequality report with `gl-code-quality-report.json` file
-   an archive with `gl-code-quality-report.html` file

This is done using Code Climate plugins and runs in two jobs.

## Jobs

| Job                 | Pipeline | Precondition | Tool        | Report Artifact     |
| ------------------- | -------- | ------------ | ----------- | ------------------- |
| `code-quality`      | branch   | src          | CodeClimate | `code-quality.json` |
| `code-quality-html` | branch   | src          | CodeClimate | `code-quality.html` |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

GitLab can display the results of one or more reports in:

-   The merge request license compliance widget.
-   if pages are used in the `report/code-quality` folder

### Configuration

| Variable      | Default | Example                             | Description      |
| ------------- | ------- | ----------------------------------- | ---------------- |
| `INLCUDE_JOB` |         | `code-quality`, `code-quality-html` | Include this job |

## Example

By default this job will not run, you have to activate it using `INCLUDE_JOB`:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml

variables:
    INCLUDE_JOB: code-quality
```
