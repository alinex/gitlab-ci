# SSH Upload/Deploy

This template will only build a base for ssh commands to deploy on the server using a SSH connection. 

## Upload

The upload will cleanup old deployments and copy the new files onto the server.

Within the given `UPLOAD_FOLDER` a directory named `<project>-<tag>-<pipeline-id>` will be created. It also includes a `version.info` file.

```text title="version.info"
NAME        betrieb/operations
REVISION    1b692ea535794e79449f8cef029948a933aea7c8
TAG         v1.9.0
LANGUAGES   typescript,shell,javascript,vim script
PIPELINE    https://gitlab.com/alinex/operations/-/pipelines/4728
USER        alinex <info@alinex.de>
DATE        2021-12-30T14:35:08+01:00
PROJECT     https://gitlab.com/alinex/operations
REPOSITORY  http://gitlab-ci-token:[MASKED]@gitlab.com/alinex/operations.git
```

If not already there the `UPLOAD_PARTS` will be moved to a `dist` folder.

## Deploy

Within the deployment the following steps will be done:

-   check if directory is still there
-   copy directory
-   replace current with new version
-   write install log

While within a freeze time (set in Settings > CI/CD > Deploy freezes) the production deploy will be not made automatically and you have to do this manually.

## Jobs

| Job              | Pipeline      | Precondition | Tool | Report Artifact |
| ---------------- | ------------- | ------------ | ---- | --------------- |
| `upload-develop` | develop       |              | ssh  | -               |
| `switch-develop` | develop       |              | ssh  | -               |
| `upload-main`    | main/master   |              | ssh  | -               |
| `switch-main`    | main/master   |              | ssh  | -               |
| `upload-prod`    | protected tag |              | ssh  | -               |
| `switch-prod`    | protected tag |              | ssh  | -               |

### Configuration

| Variable                | Default                            | Example                               | Description                                           |
| ----------------------- | ---------------------------------- | ------------------------------------- | ----------------------------------------------------- |
| `EXCLUDE_JOB`           |                                    | `upload`, `deploy`, `upload-main`...  | Exclude this job                                      |
| `SSH_PRIVATE_KEY`       |                                    | `----BEGIN RSA PRIVATE KEY-----\n...` | The private ssh key to use for connections (mask it). |
| `UPLOAD_PARTS`          | `bin config data lib node_modules` |                                       | Directories or files to upload (if no dist dir)       |
| `UPLOAD_KEEP_OLD`       | 3                                  |                                       | Number of old uploads to keep.                        |
| `DEPLOY_WAIT`           | 20                                 |                                       | Seconds to wait after each deploy.                    |
| `UPLOAD_FOLDER`         | `/opt/divibib/upload`              |                                       | Folder the code will be uploaded to.                  |
| `PROD_UPLOAD_SSH`       |                                    | `admin@operations.host.test.dvb`      | SSH connection string list                            |
| `PROD_UPLOAD_FOLDER`    | `/opt/divibib/upload`              |                                       | Folder the code will be uploaded to.                  |
| `PROD_DEPLOY_PATH`      | `/opt/divibib/<project-name>`      |                                       | Path there the application will be deployed.          |
| `MAIN_UPLOAD_SSH`       |                                    | `admin@operations.host.staging.dvb`   | SSH connection string list                            |
| `MAIN_UPLOAD_FOLDER`    | `/opt/divibib/upload`              |                                       | Folder the code will be uploaded to.                  |
| `MAIN_DEPLOY_PATH`      | `/opt/divibib/<project-name>`      |                                       | Path there the application will be deployed.          |
| `DEVELOP_UPLOAD_SSH`    |                                    | `admin@operations.host.test.dvb`      | SSH connection string list                            |
| `DEVELOP_UPLOAD_FOLDER` | `/opt/divibib/upload`              |                                       | Folder the code will be uploaded to.                  |
| `DEVELOP_DEPLOY_PATH`   | `/opt/divibib/<project-name>`      |                                       | Path there the application will be deployed.          |
| `BEFORE_DEPLOY`         |                                    | `sudo systemctl stop myapp`           | Run this additional command before deploying.         |
| `AFTER_DEPLOY`          |                                    | `sudo systemctl start myapp`          | Run this additional command after deploying.          |

All Settings with no default value need to be provided if the specific jobs should run.
The `PROD_*`, `MAIN_*`and `DEVELOP_*` variables should be defined in the UI. There you can set multiple `*_UPLOAD_SSH` paths in a variable separated by space or newlines.

## Example

This is loaded by `all.yml` but you need to configure the variables to be activated, too.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: http://gitlab.service.office.grp/templates/gitlab-ci/-/raw/main/all.yml
```

### Internal structure

It has three templates to be used:

-   `.ssh` - basic ssh action
-   `.upload-ssh` - base for upload
-   `.switch-ssh` - base for switch

The following jobs are supported:

You need to set the tags which should upload to production as protected under Configuration > Repository > Protected Tags and use something like `v*`.
This also means that only maintainers are able to set the tag and start the pipeline.

As shown above you need the same variables as used for the upload:

### Individual upload/deploy

```yaml title=".gitlab-ci.yml"
upload-self:
    extends: .upload-ssh
    variables:
        UPLOAD_SSH: $PROD_UPLOAD_SSH
        UPLOAD_FOLDER: $PROD_UPLOAD_FOLDER
    rules:
        - if: $PROD_UPLOAD_SSH == ""
          when: never
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_TAG

switch-self:
    extends: .switch-ssh
    variables:
        UPLOAD_SSH: $PROD_UPLOAD_SSH
        UPLOAD_FOLDER: $PROD_UPLOAD_FOLDER
        DEPLOY_PATH: $PROD_DEPLOY_PATH
    needs:
        - job: upload-self
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_TAG
```

### Own scripts

```yaml title=".gitlab-ci.yml"
deploy:
    extends: .ssh
    needs:
        - job: build-nodejs
          optional: true
    script:
        -  # run your deploy script here
    rules:
        - if: $PROD_UPLOAD_SSH == ""
          when: never
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_TAG
```
