# Secret Detection

The secret-detection report collects detected secrets. The collected Secret Detection report is uploaded to GitLab.
It will create a report named `secret_detection` as artifact containing `gl-secret-detection-report.json`.

!!! note

    See also the [GitLab documentation](https://docs.gitlab.com/ee/user/application_security/secret_detection/).

Secrets include keys, passwords, API tokens, and other sensitive information.
Secret Detection uses the Gitleaks tool to scan the repository for secrets within the last changes. See the [ruleset](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/gitleaks.toml) which is sued.

## Jobs

| Job      | Pipeline | Precondition | Tool                                                | Report Artifact |
| -------- | -------- | ------------ | --------------------------------------------------- | --------------- |
| `secret` | branch   | -            | [GitLeaks](https://github.com/zricethezav/gitleaks) | secret.json     |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

GitLab can display the results of one or more reports in:

-   The merge request secret scanning widget.
-   The pipeline Security tab.
-   The security dashboard: "Security & Compliance" > "Security Dashboard"

### Configuration

| Variable                         | Default | Example  | Description                                  |
| -------------------------------- | ------- | -------- | -------------------------------------------- |
| `INLCUDE_JOB`                    |         | `secret` | Include this job                             |
| `SECRET_DETECTION_HISTORIC_SCAN` | false   | true     | Scanning the full history of the repository. |

## Example

By default this job will not run, you have to activate it using `INCLUDE_JOB`:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml

variables:
    INCLUDE_JOB: secret
```
