# Static Application Security Testing

This will check the source code for known vulnerabilities.

The results are sorted by the priority of the vulnerability: Critical, High, Medium, Low, Info, Unknown

!!! note

    See also the [GitLab documentation](https://docs.gitlab.com/ee/user/application_security/sast/).

## Jobs

| Job                         | Pipeline | Precondition                                            | Tool                 | Report Artifact               |
| --------------------------- | -------- | ------------------------------------------------------- | -------------------- | ----------------------------- |
| `sast-bandit`               | branch   | `*.py`                                                  | bandit               | sast-bandit.json              |
| `sast-brakeman`             | branch   | `*.rb`, `Gemfile`                                       | brakeman             | sast-brakeman.json            |
| `sast-eslint`               | branch   | `*.html`, `*.js`, `*.jsx`, `*.ts`, `*.tsx`              | eslint               | sast-eslin.jsont              |
| `sast-flawfinder`           | branch   | `*.c`, `*.cpp`                                          | flawfinder           | sast-flawfinder.json          |
| `sast-kubesec`              | branch   | `$SCAN_KUBERNETES_MANIFESTS == 'true'`                  | kubesec              | sast-kubesec.json             |
| `sast-gosec`                | branch   | `*.go`                                                  | gosec                | sast-gosec.json               |
| `sast-mobsf-android`        | branch   | `*.apk`, `AndroidManifest.xml`                          | mobsf-android        | sast-mobsf-android.json       |
| `sast-mobsf-ios`            | branch   | `*.ipa`, `*.xcodeproj/`                                 | mobsf-ios            | sast-mobsf-ios.json           |
| `sast-nodejs-scan-`         | branch   | `package.json`                                          | nodejs-scan          | sast-nodejs-scan.json         |
| `sast-phpcs-security-audit` | branch   | `*.php`                                                 | phpcs-security-audit | sast-phpcs-security-audi.json |
| `sast-pmd-apex`             | branch   | `*.cls`                                                 | pmd-apex             | sast-pmd-apex.json            |
| `sast-security-code-scan`   | branch   | `*.csproj`, `*.vbproj`                                  | security-code-scan   | sast-security-code-scan.json  |
| `sast-semgrep`              | branch   | `*.py`, `*.js`, `*.jsx`, `*.ts`, `*.tsx`, `*.c`, `*.go` | semgrep              | sast-semgrep.json             |
| `sast-sobelow`              | branch   | `mix.exs`                                               | sobelow              | sast-sobelow.json             |
| `sast-spotbugs`             | branch   | `*.groovy`, `*.java`, `*.scala`, `*.kt`                 | spotbugs             | sast-spotbugs.json            |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

The collected SAST report uploads to GitLab as an artifact:

-   report with `gl-sast-report.json` file

This can be shown:

-   in the merge request SAST widget
-   in the security dashboard
-   in the vulnerability report

### Configuration

| Variable      | Default | Example | Description         |
| ------------- | ------- | ------- | ------------------- |
| `INCLUDE_JOB` |         | `sast`  | Include these jobs. |

## Example

By default this job will not run, you have to activate it using `INCLUDE_JOB`:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml

variables:
    INCLUDE_JOB: sast
```
