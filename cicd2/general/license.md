# License compliance

License Compliance to search your project’s dependencies for their licenses.
It will create a report named `license_scanning` as artifact containing `gl-license-scanning-report.json`.

!!! note

    See also the [GitLab documentation](https://docs.gitlab.com/ee/user/compliance/license_compliance/).

## Jobs

| Job       | Pipeline | Precondition | Tool                                                      | Report Artifact |
| --------- | -------- | ------------ | --------------------------------------------------------- | --------------- |
| `license` | branch   | -            | [LicenseFinder](https://github.com/pivotal/LicenseFinder) | license.json    |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

GitLab can display the results of one or more reports in:

-   The merge request license compliance widget.
-   The license list: "Security & Compliance" > "License compliance"

### Configuration

| Variable                  | Default | Example                                                                               | Description                                                                |
| ------------------------- | ------- | ------------------------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| `INCLUDE_JOB`             |         | `license`                                                                             | Include this job                                                           |
| `LICENSE_FINDER_CLI_OPTS` |         | `--recursive`                                                                         | If you have multiple projects in nested directories enable recursive scan. |
| `MAVEN_CLI_OPTS`          |         | `--settings mysettings.xml -Drepository.password=verysecret -Drepository.user=myuser` | private Maven repository                                                   |

## Supported Scanner

The following languages and package managers are supported.

| Language           | Package managers                                                     |
| ------------------ | -------------------------------------------------------------------- |
| C++/C              | Conan                                                                |
| Elixir             | Mix                                                                  |
| Erlang             | Rebar                                                                |
| Go                 | Godep (deprecated), go mod, go get, gvt, glide, dep, trash, govendor |
| Java               | Gradle (2 and later), Maven (3.2.5 and later)                        |
| JavaScript         | Bower, npm (7 and earlier), yarn                                     |
| Objective-C, Swift | Carthage, CocoaPods v0.39 and below                                  |
| PHP                | Composer                                                             |
| Python             | pip                                                                  |
| Ruby               | gem                                                                  |
| Rust               | Cargo                                                                |
| .NET               | NuGet                                                                |

## Example

By default this job will not run, you have to activate it using `INCLUDE_JOB`:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml

variables:
    INCLUDE_JOB: license
```
