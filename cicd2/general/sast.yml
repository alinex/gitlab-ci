# NodeJS Unit Test

# Read more about this feature here: https://docs.gitlab.com/ee/user/application_security/sast/
#
# Configure SAST with CI/CD variables (https://docs.gitlab.com/ee/ci/variables/index.html).
# List of available variables: https://docs.gitlab.com/ee/user/application_security/sast/index.html#available-variables

variables:
  SCAN_KUBERNETES_MANIFESTS: "false"
  SAST_EXCLUDED_PATHS: "spec, test, tests, tmp, doc, node_modules"

.sast:
  variables:
    SEARCH_MAX_DEPTH: 4
  stage: quality
  needs: []
  script:
    - mkdir -p ci-report
    - /analyzer run
    - test ! -e gl-sast-report.json || cp gl-sast-report.json ci-report/$CI_JOB_NAME.json
  allow_failure: true
  interruptible: true
  artifacts:
    paths: [ci-report/]
    reports:
      sast: gl-sast-report.json

sast-bandit:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/bandit:2
  before_script:
    - |
      if $(ĺs src/**/*.py >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/*.py 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-bandit)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-bandit)/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'src/**/*.py'

sast-brakeman:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/brakeman:2
  before_script:
    - |
      if $(ĺs src/**/*.rb src/**/Gemfile >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/*.rb src/**/Gemfile 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-brakeman)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-brakeman)/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'src/**/*.rb'
        - 'src/**/Gemfile'

sast-eslint:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/eslint:2
  before_script:
    - |
      if $(ĺs src/**/*.html src/**/*.js src/**/*.jsx src/**/*.ts src/**/*.tsx >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/*.html src/**/*.js src/**/*.jsx src/**/*.ts src/**/*.tsx 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-eslint)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-eslint)/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'src/**/*.html'
        - 'src/**/*.js'
        - 'src/**/*.jsx'
        - 'src/**/*.ts'
        - 'src/**/*.tsx'

sast-flawfinder:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/flawfinder:2
  before_script:
    - |
      if $(ĺs src/**/*.c src/**/*.cpp >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/*.c src/**/*.cpp 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-flawfinder)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-flawfinder)/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'src/**/*.c'
        - 'src/**/*.cpp'

sast-kubesec:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/kubesec:2
  before_script:
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-kubesec)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-kubesec)/
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $SCAN_KUBERNETES_MANIFESTS == 'true'

sast-gosec:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/gosec:3
  before_script:
    - |
      if $(ĺs src/**/*.go >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/*.go 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-gosec)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-gosec)/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'src/**/*.go'

.sast-mobsf:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/mobsf:2

sast-mobsf-android:
  extends: .sast-mobsf
  before_script:
    - |
      if $(ĺs src/**/*.apk src/**/AndroidManifest.xml >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/*.apk src/**/AndroidManifest.xml 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-mobsf|-mobsf-android)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-mobsf|-mobsf-android)/
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $SAST_EXPERIMENTAL_FEATURES == 'true'
      exists:
        - 'src/**/*.apk'
        - 'src/**/AndroidManifest.xml'

sast-mobsf-ios:
  extends: .sast-mobsf
  before_script:
    - |
      if $(ĺs src/**/*.ipa src/**/*.xcodeproj/* >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/*.ipa src/**/*.xcodeproj/* 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-mobsf|-mobsf-ios)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-mobsf|-mobsf-ios)/
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $SAST_EXPERIMENTAL_FEATURES == 'true'
      exists:
        - 'src/**/*.ipa'
        - 'src/**/*.xcodeproj/*'

sast-nodejs-scan:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan:2
  before_script:
    - |
      if $(ĺs src/**/package.json >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/package.json 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-nodejs)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-nodejs)/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'src/**/package.json'

sast-phpcs-security-audit:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit:2
  before_script:
    - |
      if $(ĺs src/**/*.php >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/*.php 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-phpcs)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-phpcs)/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'src/**/*.php'

sast-pmd-apex:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/pmd-apex:2
  before_script:
    - |
      if $(ĺs src/**/*.cls >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/*.cls 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-pmd)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-pmd)/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'src/**/*.cls'

sast-security-code-scan:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/security-code-scan:2
  before_script:
    - |
      if $(ĺs src/**/*.csproj src/**/*.vbproj >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/*.csproj src/**/*.vbproj 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-security)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-security)/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'src/**/*.csproj'
        - 'src/**/*.vbproj'

sast-semgrep:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/semgrep:2
  before_script:
    - |
      if $(ĺs src/**/*.py src/**/*.js src/**/*.jsx src/**/*.ts src/**/*.tsx src/**/*.c src/**/*.go >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/*.py src/**/*.js src/**/*.jsx src/**/*.ts src/**/*.tsx src/**/*.c src/**/*.go 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
    - ulimit -s 34359738368
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-semgrep)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-semgrep)/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'src/**/*.py'
        - 'src/**/*.js'
        - 'src/**/*.jsx'
        - 'src/**/*.ts'
        - 'src/**/*.tsx'
        - 'src/**/*.c'
        - 'src/**/*.go'

sast-sobelow:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/sobelow:2
  before_script:
    - |
      if $(ĺs mix.exs >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 mix.exs 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-sobelow)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-sobelow)/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'mix.exs'

sast-spotbugs:
  extends: .sast
  image: registry.gitlab.com/gitlab-org/security-products/analyzers/spotbugs:2
  before_script:
    - |
      if $(ĺs src/**/*.groovy src/**/*.java src/**/*.scala src/**/*.kt >/dev/null 2>&1); then
        echo "Skipping Job because precondition not reached!"
        exit 33
      fi
    - |
      # output setup information
      echo "========================================================================================="
      echo "The following files are found:"
      ls -1 src/**/*.groovy src/**/*.java src/**/*.scala src/**/*.kt 2>/dev/null | sed 's/^/- /' || true
      echo "========================================================================================="
  rules:
    - if: $EXCLUDE_JOB =~ /sast($|\s|-spotbugs)/
      when: never
    - if: $INCLUDE_JOB !~ /sast($|\s|-spotbugs)/
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - 'src/**/*.groovy'
        - 'src/**/*.java'
        - 'src/**/*.scala'
        - 'src/**/*.kt'
