# Deploy using SSH

variables:
  # general setup
  SSH_PRIVATE_KEY:
    description: The private ssh key to use for connections (mask it).
  UPLOAD_PARTS: ""
  UPLOAD_KEEP_OLD:
    value: 3
    description: Number of older deploys to keep.
  DEPLOY_WAIT:
    value: 20
    description: Wait seconds after each deploy.
  # production server
  PROD_UPLOAD_SSH: 
    value: ""
    description: "SSH connection string like `user@host.office.dvb`."
  PROD_DEPLOY_PATH:
    value: ""
    description: Path to deploy the application to.
  # staging/test server connected to main branch
  MAIN_UPLOAD_SSH: 
    value: ""
    description: "SSH connection string like `user@host.staging.dvb`."
  MAIN_DEPLOY_PATH:
    value: ""
    description: Path to deploy the application to.
  # test server connected to develop branch
  DEVELOP_UPLOAD_SSH: 
    value: ""
    description: "SSH connection string like `user@host.test.dvb`."
  DEVELOP_DEPLOY_PATH:
    value: ""
    description: Path to deploy the application to.
  # will mostly be set by specific ones
  UPLOAD_SSH: ""
  UPLOAD_FOLDER: ""
  UPLOAD_FOLDER_DEFAULT: /opt/divibib/upload
  DEPLOY_PATH: ""

.ssh:
  image: btmash/alpine-ssh-rsync
  before_script: &before_script
    # setup ssh
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
  interruptible: false

.upload-ssh:
  extends: .ssh
  before_script:
    - *before_script
    # check variables
    - test -n "$UPLOAD_SSH" || echo "UPLOAD_SSH not specified!"
    - test -n "$UPLOAD_SSH" # fail
    - UPLOAD_FOLDER=${UPLOAD_FOLDER:-$UPLOAD_FOLDER_DEFAULT}
    - test -n "$UPLOAD_FOLDER" || echo "UPLOAD_FOLDER not specified!"
    - test -n "$UPLOAD_FOLDER" # fail
    # build version ID
    - apk update && apk add --no-cache git
    - git fetch --depth=10000
    - LAST_TAG=$(git describe --tags --abbrev=0 2>/dev/null || echo '')
    - ID=${LAST_TAG:-$CI_COMMIT_BRANCH}-$CI_PIPELINE_ID
    # define upload parts
    - test -n "$UPLOAD_PARTS" || test -d dist || UPLOAD_PARTS="bin config data lib node_modules package.json"  
     # make version.info file
    - echo "NAME        $CI_PROJECT_PATH" >> version.info
    - echo "REVISION    $CI_COMMIT_SHA" >> version.info
    - echo "TAG         $LAST_TAG" >> version.info
    - echo "LANGUAGES   $CI_PROJECT_REPOSITORY_LANGUAGES" >> version.info
    - echo "PIPELINE    $CI_PIPELINE_URL" >> version.info
    - echo "USER        $GITLAB_USER_NAME" >> version.info
    - echo "DATE        $CI_JOB_STARTED_AT" >> version.info
    - echo "PROJECT     $CI_PROJECT_URL" >> version.info
    - echo "REPOSITORY  $CI_REPOSITORY_URL" >> version.info
    # output info
    - |
      # output upload information
      echo "========================================================================================="
      for SSH in $UPLOAD_SSH; do
        echo "Upload to:   $SSH"
      done
      test -z "$UPLOAD_PARTS" || echo "Folders:     $UPLOAD_PARTS"
      test -n "$UPLOAD_PARTS" || echo "Files from:  dist/*"
      echo "Destination: $UPLOAD_FOLDER/$CI_PROJECT_NAME-$ID..."
      echo "Keep the last $UPLOAD_KEEP_OLD entries."
      echo "-----------------------------------------------------------------------------------------"
      cat version.info
      echo "========================================================================================="
  script:
    - |
      if [ ! -d dist ]; then
        echo "Building distribution"
        mkdir dist
        mv $UPLOAD_PARTS dist 2>/dev/null || true
      fi
    - ls -al dist
    - |
      for SSH in $UPLOAD_SSH; do
        echo "Upload to: $SSH"
        # create upload folder if missing
        ssh $SSH mkdir -p $UPLOAD_FOLDER
        # cleanup
        ssh $SSH find $UPLOAD_FOLDER -maxdepth 1 -name $CI_PROJECT_NAME-\\* \| xargs ls -dt \| tail -n +$UPLOAD_KEEP_OLD \| xargs rm -rf
        # transfer
        rsync -a --no-perms dist/ version.info $SSH:$UPLOAD_FOLDER/$CI_PROJECT_NAME-$ID
      done

.switch-ssh:
  extends: .ssh
  image: btmash/alpine-ssh-rsync
  before_script:
    - *before_script
    # check variables
    - test -n "$UPLOAD_SSH" || echo "UPLOAD_SSH not specified!"
    - test -n "$UPLOAD_SSH" # fail
    - UPLOAD_FOLDER=${UPLOAD_FOLDER:-$UPLOAD_FOLDER_DEFAULT}
    - test -n "$UPLOAD_FOLDER" || echo "UPLOAD_FOLDER not specified!"
    - test -n "$UPLOAD_FOLDER" # fail
    # define deploy environment
    - DEPLOY_PATH=${DEPLOY_PATH:-/opt/divibib/$CI_PROJECT_NAME}
    - DEPLOY_NAME=$(basename $DEPLOY_PATH)
    - SSH=$(echo $UPLOAD_SSH | head -n1 | cut -d " " -f1)
    - SOURCE=$(ssh $SSH echo "$UPLOAD_FOLDER/$CI_PROJECT_NAME-*-$CI_PIPELINE_ID")
    - if ! ssh $SSH test -d $SOURCE; then echo "Source not found under $SOURCE" && exit 1; fi
    # output info
    - |
      # output deploy information
      echo "========================================================================================="
      echo "Source: $SOURCE"
      for SSH in $UPLOAD_SSH; do
        echo "Deploy $SSH:$SOURCE"
      done
      echo "Wait for $DEPLOY_WAIT seconds before going on."
      echo "-----------------------------------------------------------------------------------------"
      ssh $SSH cat $SOURCE/version.info # from last server only
      echo "========================================================================================="
  script: 
    - |
      LIST=$(echo "$UPLOAD_SSH" | xargs) # trim whitespace
      for SSH in $LIST; do
        # before script
        if [ -n "$BEFORE_DEPLOY" ]; then
          echo "Running remote script: $BEFORE_DEPLOY"
          ssh $SSH "$BEFORE_DEPLOY"
        fi
        # switch
        echo "Deploy $SSH:$SOURCE"
        ssh $SSH cp -r $SOURCE $DEPLOY_PATH.new # copy release
        ssh $SSH "rm -rf $DEPLOY_PATH && mv $DEPLOY_PATH.new $DEPLOY_PATH" # switch
        # central log path
        ssh $SSH mkdir -p /var/log/divibib/$DEPLOY_NAME
        ssh $SSH rm -f $SOURCE/log
        ssh $SSH ln -s /var/log/divibib/$DEPLOY_NAME $DEPLOY_PATH/log
        # add log entry
        echo -e "# $(date +"%Y-%m-%d %H:%M:%S") $(basename $SOURCE) deployed by $GITLAB_USER_NAME\nrm -rf $DEPLOY_PATH; cp -r $SOURCE $DEPLOY_PATH\n" | ssh $SSH tee -a /opt/divibib/install-ssh.log
        # after script^
        if [ -n "$AFTER_DEPLOY" ]; then
          echo "Running remote script: $AFTER_DEPLOY"
          ssh $SSH "$AFTER_DEPLOY"
        fi
        # done, wait
        sleep $DEPLOY_WAIT
      done

### upload

upload-develop:
  stage: deploy-test
  extends: .upload-ssh
  variables:
    UPLOAD_SSH: $DEVELOP_UPLOAD_SSH
    UPLOAD_FOLDER: $DEVELOP_UPLOAD_FOLDER
  rules:
    - if: $EXCLUDE_JOB =~ /upload($|\s|-develop)/
      when: never
    - if: $DEVELOP_UPLOAD_SSH == null || $DEVELOP_UPLOAD_SSH == ""
      when: never
    - if: $CI_COMMIT_BRANCH == "develop"

upload-main:
  stage: deploy-stage
  extends: .upload-ssh
  variables:
    UPLOAD_SSH: $MAIN_UPLOAD_SSH
    UPLOAD_FOLDER: $MAIN_UPLOAD_FOLDER
  rules:
    - if: $EXCLUDE_JOB =~ /upload($|\s|-main)/
      when: never
    - if: $MAIN_UPLOAD_SSH == null || $MAIN_UPLOAD_SSH == ""
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

upload-prod:
  stage: deploy-prod
  extends: .upload-ssh
  variables:
    UPLOAD_SSH: $PROD_UPLOAD_SSH
    UPLOAD_FOLDER: $PROD_UPLOAD_FOLDER
  rules:
    - if: $EXCLUDE_JOB =~ /upload($|\s|-prod)/
      when: never
    - if: $PROD_UPLOAD_SSH == null || $PROD_UPLOAD_SSH == ""
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

### deploy

switch-develop:
  stage: deploy-test
  extends: .deploy-ssh
  variables:
    UPLOAD_SSH: $DEVELOP_UPLOAD_SSH
    UPLOAD_FOLDER: $DEVELOP_UPLOAD_FOLDER
    DEPLOY_PATH: $DEVELOP_DEPLOY_PATH
  needs: 
    - job: upload-develop
  rules:
    - if: $EXCLUDE_JOB =~ /switch($|\s|-develop)/
      when: never
    - if: $DEVELOP_UPLOAD_SSH == null || $DEVELOP_UPLOAD_SSH == ""
      when: never
    - if: $CI_COMMIT_BRANCH == "develop"

switch-main:
  stage: deploy-stage
  extends: .switch-ssh
  variables:
    UPLOAD_SSH: $MAIN_UPLOAD_SSH
    UPLOAD_FOLDER: $MAIN_UPLOAD_FOLDER
    DEPLOY_PATH: $MAIN_DEPLOY_PATH
  needs: 
    - job: upload-main
  rules:
    - if: $EXCLUDE_JOB =~ /switch($|\s|-main)/
      when: never
    - if: $MAIN_UPLOAD_SSH == null || $MAIN_UPLOAD_SSH == ""
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

switch-prod:
  stage: deploy-prod
  extends: .switch-ssh
  variables:
    UPLOAD_SSH: $PROD_UPLOAD_SSH
    UPLOAD_FOLDER: $PROD_UPLOAD_FOLDER
    DEPLOY_PATH: $PROD_DEPLOY_PATH
  needs: 
    - job: upload-prod
  rules:
    - if: $EXCLUDE_JOB =~ /switch($|\s|-prod)/
      when: never
    - if: $PROD_UPLOAD_SSH == null || $PROD_UPLOAD_SSH == ""
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_DEPLOY_FREEZE
      when: manual
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual # remove to auto deploy
