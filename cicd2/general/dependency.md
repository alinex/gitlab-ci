# Dependency scanning

This template will run dependency scanning for different languages and create reports. It lets you know if your application uses an external (open source) library that is known to be vulnerable.
It will create a report named `dependency_scanning` as artifact containing `gl-dependency-scanning-report.json`.

!!! note

    See also the [GitLab documentation](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/).

These results will be visible in:

-   The merge request dependency scanning widget.
-   The pipeline Security tab.
-   The security dashboard: "Security & Compliance" > "Security dashboard"
-   The Project Vulnerability report: "Security & Compliance" > "Vulnerability report"
-   The dependency list: "Security & Compliance" > "Dependency list"

# License compliance

License Compliance to search your project’s dependencies for their licenses.
It will create a report named `license_scanning` as artifact containing `gl-license-scanning-report.json`.

!!! note

    See also the [GitLab documentation](https://docs.gitlab.com/ee/user/compliance/license_compliance/).

## Jobs

| Job                    | Pipeline | Precondition                                                                                                                                                                                                                                                                                                                                                                                                                                | Tool                                                                     | Report Artifact           |
| ---------------------- | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------ | ------------------------- |
| `dependency-gemnasium` | branch   | `Gemfile.lock */Gemfile.lock */*/Gemfile.lock composer.lock */composer.lock */*/composer.lock gems.locked */gems.locked */*/gems.locked go.sum */go.sum */*/go.sum npm-shrinkwrap.json */npm-shrinkwrap.json */*/npm-shrinkwrap.json package-lock.json */package-lock.json */*/package-lock.json yarn.lock */yarn.lock */*/yarn.lock packages.lock.json */packages.lock.json */*/packages.lock.json conan.lock */conan.lock */*/conan.lock` | [Gemnasium](<[https](http://docs.gemnasium.net/en/stable/welcome.html)>) | dependency-gemnasium.json |
| `dependency-maven`     | branch   | `build.gradle */build.gradle */*/build.gradle build.gradle.kts */build.gradle.kts */*/build.gradle.kts build.sbt */build.sbt */*/build.sbt pom.xml */pom.xml */*/pom.xml `                                                                                                                                                                                                                                                                  | [Maven](<[https](https://maven.apache.org/)>)                            | dependency-maven.json     |
| `dependency-python`    | branch   | `Gemfile.lock */Gemfile.lock */*/Gemfile.lock`                                                                                                                                                                                                                                                                                                                                                                                              | Python                                                                   | dependency-python.json    |
| `dependency-bundler`   | branch   | `Gemfile.lock */Gemfile.lock */*/Gemfile.lock`                                                                                                                                                                                                                                                                                                                                                                                              | [Bundler](<[https](https://bundler.io/)>)                                | dependency-bundler.json   |
| `dependency-retirejs`  | branch   | `package.json */package.json */*/package.json`                                                                                                                                                                                                                                                                                                                                                                                              | [LicenseFinder](https)                                                   | dependency-retirejs.json  |

All reports are made for GitLab to be displayed in the UI (if your license include it) and also in the artifacts.

GitLab can display the results of one or more reports in:

-   The merge request license compliance widget.
-   The license list: "Security & Compliance" > "License compliance"

### Configuration

| Variable      | Default | Example                   | Description      |
| ------------- | ------- | ------------------------- | ---------------- |
| `EXCLUDE_JOB` |         | `dependency dependency-*` | Exclude this job |

## Scanners

| Language   | Versions                 | Package Manager | Supported files                                | Analyzer      |
| ---------- | ------------------------ | --------------- | ---------------------------------------------- | ------------- |
| Ruby       | N/A                      | Bundler         | Gemfile.lock gems.locked                       | Gemnasium     |
| Ruby       | N/A                      | Bundler         | Gemfile.lock                                   | bundler-audit |
| PHP        | N/A                      | Composer        | composer.lock                                  | Gemnasium     |
| C          | N/A                      | Conan           | conan.lock                                     | Gemnasium     |
| C++        | N/A                      | Conan           | conan.lock                                     | Gemnasium     |
| Go         | N/A                      | Go              | go.sum                                         | Gemnasium     |
| Java       | 8, 11, 13, 14, 15, or 16 | Gradle          | build.gradle build.gradle.kts                  | Gemnasium     |
| Java       | 8, 11, 13, 14, 15, or 16 | Maven           | pom.xml                                        | Gemnasium     |
| JavaScript | N/A                      | npm             | package-lock.json npm-shrinkwrap.json          | Gemnasium     |
| JavaScript | N/A                      | npm             | package.json                                   | Retire.js     |
| JavaScript | N/A                      | yarn            | yarn.lock                                      | Gemnasium     |
| .NET       | N/A                      | NuGet           | packages.lock.json                             | Gemnasium     |
| C#         | N/A                      | NuGet           | packages.lock.json                             | Gemnasium     |
| Python     | 3.6                      | setuptools      | setup.py                                       | Gemnasium     |
| Python     | 3.6                      | pip             | requirements.txt requirements.pip requires.txt | Gemnasium     |
| Python     | 3.6                      | Pipenv          | Pipfile Pipfile.lock2                          | Gemnasium     |
| Scala      | N/A                      | sbt3            | build.sbt                                      | Gemnasium     |

GitLab analyzers obtain dependency information using one of the following two methods:

1. Parsing lockfiles directly.
2. Running a package manager or build tool to generate a dependency information file which is then parsed.

## Example

It is automatically included using `all.yml` but you can load it yourself, too. The job will only be included if the tool specific test files are present.

```yaml title=".gitlab-ci.yml" hl_lines="5"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml
```

If you want to include only the parts you wish:

```yaml title=".gitlab-ci.yml"
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/cicd/stages.yml
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/cicd/general/dependency.yml
```
