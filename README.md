# gitlab-ci

Templates for GitLab CI/CD Tasks

This is a collection of different templates to make it easier to do CI/CD tasks. They are made for the alinex repositories, but you may also use them or create your own templates like these.

The goal of this is to make a unique pipeline for all projects which will automatically do what is needed for each project and can be easily configured.

## Stages

This templates use the following stages:

-   `.pre` - only used for some debugging
-   `check` - used for functional tests on the source code
-   `quality` - used for quality checks on the source code
-   `build` - building, packaging and storing the code in repositories
-   `deploy-test` - upload, deploy and activate on server
-   `review-test` - app on server: tests
-   `deploy-stage` - upload, deploy and activate on server
-   `review-stage` - app on server: tests
-   `deploy-prod` - upload, deploy and activate on server
-   `review-prod` - app on server: tests
-   `load-prod` - load tests on app
-   `.post` - no real action but collecting all results together for pages, email and more

They include the following Jobs:

| Stage     | Active | Template                                     | Stop  | Description                                 |
| --------- | :----: | -------------------------------------------- | :---: | ------------------------------------------- |
|           |   X    | [stages](cicd/stages.md)                     |   -   | Define all possible stages                  |
| .pre      |   -    | [printenv](cicd/printenv.md)                 |   -   | Print all environment variables (debugging) |
| check     |   -    | [license](cicd/general/license.md)           |   -   | License scanning                            |
| check     |   X    | [node-unit](cicd/node/unit.md)               |   X   | Unit tests with coverage report for NodeJS  |
| check     |   X    | [bash-unit](cicd/bash/unit.md)               |   X   | Bash unit tests                             |
| quality   |   X    | [dependency](cicd/general/dependency.md)     |   -   | External dependency analysis                |
| quality   |   -    | [secret](cicd/general/secret.md)             |   -   | Secret detection                            |
| quality   |   -    | [quality](cicd/general/quality.md)           |   -   | Code quality check                          |
| quality   |   -    | [sast](cicd/general/sast.md)                 |   -   | Static Application Security Testing         |
| build     |   X    | [node-build](cicd/nodejs/build.md)           |   X   | Build NodeJS app                            |
| build     |   X    | [bash-integration](cicd/bash/integration.md) |   X   | Bash integration tests                      |
| build     |   X    | [docker-build](cicd/docker/build.md)         |   X   | Build docker image                          |
| build     |   X    | [docker-security](cicd/docker/security.md)   |   X   | Docker Security Check                       |
| build     |   X    | [docker-push](cicd/docker/push.md)           |   X   | Push Docker image to registry               |
| build     |   X    | [doc-mkdocs](cicd/doc/mkdocs.md)             |   -   | Create HTML documentation from markdown     |
| deploy-*  |   X    | [deploy](cicd/general/deploy.md)             |   X   | Deploy using SSH                            |
| review-*  |   -    | [dast](cicd/review/dast.md)                  |   X   | Dynamic Application Security Tests          |
| review-*  |   -    | [performance](cicd/review/performance.md)    |   X   | Dynamic Application Security Tests          |
| review-*  |   X    | [bash](cicd/bash/review.md)                  |   X   | Bash review tests                           |
| review-*  |   X    | [k6-review](cicd/review/k6-review.md)        |   X   | K6 review tests                             |
| load-prod |   X    | [k6-load](cicd/review/k6-load.md)            |   X   | K6 load tests                               |
| .post     |   X    | [pages](cicd/pages.md)                       |   -   | Collect Documentation and reports to pages  |

If a template is active that means it is included by default but can be excluded, else you have to include it to be used.

## Use of templates

Create a `gitlab-ci.yml` file in your repository and:

1. include: the `all.yml` template
2. variables: can be used to customize
    - include more jobs
    - exclude default jobs
    - configure jobs
3. jobs: sometimes you need special things then you can
    - overwrite the default jobs
    - add new ones

As only the first point is really necessary, you can start working with only:

```yaml
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml
```

But to get all the possible jobs running, use:

```yaml
include:
    - remote: https://gitlab.com/alinex/gitlab-ci/-/raw/main/all.yml
variables:
    INCLUDE_JOB: printenv license secret sast security-docker dast
```

The following VARIABLES are globally used for control:

| Variable      | Default | Example      | Description                                                  |
| ------------- | ------- | ------------ | ------------------------------------------------------------ |
| `EXCLUDE_JOB` |         | `dependency` | Space separated list of job names (or parts) to be excluded. |
| `INCLUDE_JOB` |         | `license`    | Include jobs, which normally are not included.               |

The `EXCLUDE_JOB` is specially helpful in developing the templates to disable something and concentrate on the rest. If you have the same job matching `INCLUDE_JOB` and `EXCLUDE_JOB` the first will take precedence and it is not integrated in the pipeline.

### Project Structure

To let this pipeline template work seamless please use the following project structure:

    git.ab-ci.yml
    src/
    test/

## Development Basics

This repository includes an `all.yml` which is the entry point to load everything.

The folder `cicd` contains the jobs in multiple files, each with a markdown documentation beside it.
Below this folder there are language specific subfolders with all jobs for each language. The jobs are named after their path to make it easy for you to find the corresponding file and job..

All other folders are identical to any development and are used to have an example application. This is used on deployment of the gitlab-ci rules itself to check that everything works fine.

### Further directories

While running the pipelines the following additional file structure is used:

    coverage/               used in unit tests (node)
    ci-report/              results in any format from tooling
    ci-pages/               collection of HTML documentation (pages)
